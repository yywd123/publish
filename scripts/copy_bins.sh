#!/bin/bash

# 执行本文件需要有root权限
# 并且需要先初始化好硬盘镜像文件

IMG=x.img

kpartx_out=`kpartx -l ${IMG}`
PART=`echo ${kpartx_out}|awk '{print $1}'`
PART=/dev/mapper/${PART}
LODEV=`echo ${kpartx_out}|awk '{print $5}'`
echo ${PART}
echo ${LODEV}

kpartx -u ${IMG}

mount $PART /mnt/

cp kernel/kernel.bin /mnt
cp kernel/kallsyms /mnt
cp kernel/modpost /mnt
cp kernel/system /mnt

cp user/init.bin /mnt
cp user/system_api_lib /mnt


umount /mnt/
kpartx -d ${LODEV}
losetup -d ${LODEV}
