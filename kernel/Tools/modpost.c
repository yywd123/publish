/***************************************************
*		 Copyright (c) 2018 MINE 田宇
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of version 2 of the GNU General Public
* License as published by the Free Software Foundation.
*
***************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

int main(int argc, char **argv)
{
	char fname[100];
	char *s = NULL;
	FILE * file = NULL;

	s = strrchr(argv[1], '.');
	if (s != NULL)
	{
		if (strcmp(s, ".o") == 0) 
		{
			*s = '\0';
		}
	}
	else
		printf("file name \"%s\" read error!\n",argv[1]);

	sprintf(fname, "%s.mod.c", argv[1]);

	file = fopen(fname,"w");

	fprintf(file, "#include \"module.h\"\n");
	fprintf(file, "\n");

	fprintf(file, "struct module __this_module__\n");
	fprintf(file, "__attribute__((section(\".this_module\"))) = \n");
	fprintf(file, "{\n");
	fprintf(file, " .init = init_module,\n");
	fprintf(file, " .exit = exit_module,\n");
	fprintf(file, " .module_name = \"%s\",\n",argv[1]);
	fprintf(file, "};\n");

	fclose(file);

	return 0;
}
