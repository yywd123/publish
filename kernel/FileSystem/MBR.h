/***************************************************
*		 Copyright (c) 2018 MINE 田宇
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of version 2 of the GNU General Public
* License as published by the Free Software Foundation.
*
***************************************************/

#ifndef __MRB_H__

#define __MRB_H__

struct Disk_Partition_Table_Entry
{
	unsigned char flags;
	unsigned char start_head;
	unsigned short  start_sector	:6,	//0~5
			start_cylinder	:10;	//6~15
	unsigned char type;
	unsigned char end_head;
	unsigned short  end_sector	:6,	//0~5
			end_cylinder	:10;	//6~15
	unsigned int start_LBA;
	unsigned int sectors_limit;
}__attribute__((packed));

struct Disk_Partition_Table
{
	unsigned char BS_Reserved[446];
	struct Disk_Partition_Table_Entry DPTE[4];
	unsigned short BS_TrailSig;
}__attribute__((packed));

#endif
