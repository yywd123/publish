/***************************************************
*		 Copyright (c) 2018 MINE 田宇
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of version 2 of the GNU General Public
* License as published by the Free Software Foundation.
*
***************************************************/

#ifndef __GPT_H__

#define __GPT_H__

#include "MBR.h"

struct GPT_Header
{
	unsigned long	Signature;
	unsigned int	Revision;
	unsigned int	HeaderSize;
	unsigned int	HeaderCRC32;
	unsigned int	Reserved;
	unsigned long	MyLBA;
	unsigned long	AlternateLBA;

	unsigned long	FirstUsableLBA;
	unsigned long	LastUsableLBA;

	unsigned long	DiskGUID[2];

	unsigned long	PartitionEntryLBA;
	unsigned int	NumberOfPartitionEntries;
	unsigned int	SizeOfPartitionEntry;
	unsigned int	PartitionEntryArrayCRC32;
}__attribute__((packed));

struct GPT_Partition_Entry
{
	unsigned long	PartitionTypeGUID[2];
	unsigned long	UniquePartitionGUID[2];

	unsigned long	StartingLBA;
	unsigned long	EndingLBA;
	unsigned long	Attributes;
	unsigned short	PartitionName[36];
}__attribute__((packed));

#endif
