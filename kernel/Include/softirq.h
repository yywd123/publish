/***************************************************
*		 Copyright (c) 2018 MINE 田宇
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of version 2 of the GNU General Public
* License as published by the Free Software Foundation.
*
***************************************************/

#ifndef __SOFTIRQ_H__

#define __SOFTIRQ_H__

#include "cpu.h"

enum
{
    HI_SOFTIRQ = 0,  /* 优先级高的 tasklet */
    TIMER_SOFTIRQ,   /* 定时器的下半部 */
    TASKLET_SOFTIRQ, /* 正常优先权的 tasklet */
};

extern unsigned long softirq_status[NR_CPUS];

struct softirq
{
	void (*action)(void * data);
	void * data;
};

extern struct softirq softirq_vector[64];


void register_softirq(int nr,void (*action)(void * data),void * data);
void unregister_softirq(int nr);

void set_softirq_status(unsigned long status);
unsigned long get_softirq_status();

void softirq_init();

#endif
