/***************************************************
*		 Copyright (c) 2018 MINE 田宇
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of version 2 of the GNU General Public
* License as published by the Free Software Foundation.
*
***************************************************/

#ifndef __MODULE_H__

#define __MODULE_H__

#include "lib.h"
#include "ELF.h"

struct module
{
	int (*init)(void);
	void (*exit)(void);
	char module_name[32];
	Elf64_Ehdr *ehdr;
	Elf64_Shdr *shdr;
	Elf64_Shdr *shstrdr;
	Elf64_Shdr *shsymtab;
	char * module_file;
	struct List list;
};

struct kernel_symbol
{
	unsigned long address;
	char *name;
};

extern int init_module(void);
extern void exit_module(void);

#define module_init(initfn)	\
int init_module(void) __attribute__((alias(#initfn)));

#define module_exit(exitfn)	\
void exit_module(void) __attribute__((alias(#exitfn)));

#define EXPORT_SYMBOL(symbol)	\
extern	typeof(symbol)	symbol;	\
char	KString_table_##symbol[]	\
__attribute__ ((section(".KString_table"),aligned(1)))	\
=	#symbol;		\
struct	kernel_symbol	KSymbol_table_##symbol		\
__attribute__ ((section(".KSymbol_table"),unused))	\
={(unsigned long)&symbol,KString_table_##symbol};

#endif
