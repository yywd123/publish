/***************************************************
*		 Copyright (c) 2018 MINE 田宇
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of version 2 of the GNU General Public
* License as published by the Free Software Foundation.
*
***************************************************/

#ifndef __BLOCK_H__

#define __BLOCK_H__

#include "lib.h"
#include "semaphore.h"

#define BLK_CMD_READ				0x01
#define BLK_CMD_WRITE				0x02
#define BLK_CMD_IDENTIFY			0x03
#define BLK_CMD_SET_FEATURES			0x04
#define BLK_CMD_CREATE_IO_COMPLETION_QUEUE	0x05
#define BLK_CMD_CREATE_IO_SUBMISSION_QUEUE	0x06


struct block_buffer_node
{
	unsigned int count;
	unsigned char cmd;
	unsigned long LBA;
	unsigned char * buffer;
	void(* end_handler)(unsigned long nr, unsigned long parameter);

	wait_queue_T wait_queue;
};

struct request_queue
{
	wait_queue_T wait_queue_list;
	struct block_buffer_node *in_using;
	long block_request_count;
};

struct block_device_operation
{
	long (* open)();
	long (* close)();
	long (* ioctl)(long cmd,long arg);
	long (* transfer)(long cmd,unsigned long blocks,long count,unsigned char * buffer);
};

#endif
