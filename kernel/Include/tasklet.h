/*
 * @Author: your name
 * @Date: 2022-03-22 17:43:59
 * @LastEditTime: 2022-03-23 14:42:51
 * @LastEditors: Please set LastEditors
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: \kernel\tasklet.h
 */
#ifndef __TASKLET_H__
#define __TASKLET_H__

#include "atomic.h"
#include "softirq.h"

enum
{
    TASKLET_STATE_SCHED, /* Tasklet is scheduled for execution */
    TASKLET_STATE_RUN    /* Tasklet is running (SMP only) */
};

struct tasklet_struct
{
    struct tasklet_struct *next; /* next tasklet in the list */
    unsigned long state;         /* state of the tasklet */
    atomic_T count;              /* reference counter */
    void (*func)(unsigned long); /* tasklet handler function */
    unsigned long data;          /* argument to the tasklet handler */
};

struct tasklet_head
{
    struct tasklet_struct *list;
};

static inline long tasklet_trylock(struct tasklet_struct *t)
{
    return !test_and_set_bit(TASKLET_STATE_RUN, &(t)->state);
}

static inline void tasklet_unlock(struct tasklet_struct *t)
{
    barrier();
    clear_bit(TASKLET_STATE_RUN, &(t)->state);
}

void tasklet_init(struct tasklet_struct *t, void (*func)(unsigned long), unsigned long data);
void tasklet_schedule(struct tasklet_struct *t);
void tasklet_hi_schedule(struct tasklet_struct *t);
void tasklet_action(void *a);
void tasklet_hi_action(void *a);

#endif
