/***************************************************
*		 Copyright (c) 2018 MINE 田宇
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of version 2 of the GNU General Public
* License as published by the Free Software Foundation.
*
***************************************************/

#ifndef __ATOMIC_H__

#define __ATOMIC_H__

#include "linkage.h"

typedef struct
{
	__volatile__ long value;
} atomic_T;

#define atomic_read(atomic)	((atomic)->value)
#define atomic_set(atomic,val)	(((atomic)->value) = (val))

__always_inline__ void atomic_add(atomic_T * atomic,long value)
{
	__asm__ __volatile__	(	"lock	addq	%1,	%0	\n\t"
					:"=m"(atomic->value)
					:"r"(value)
					:"memory"
				);
}

__always_inline__ void atomic_sub(atomic_T *atomic,long value)
{
	__asm__ __volatile__	(	"lock	subq	%1,	%0	\n\t"
					:"=m"(atomic->value)
					:"r"(value)
					:"memory"
				);
}

__always_inline__ void atomic_inc(atomic_T *atomic)
{
	__asm__ __volatile__	(	"lock	incq	%0	\n\t"
					:"=m"(atomic->value)
					:"m"(atomic->value)
					:"memory"
				);
}

__always_inline__ void atomic_dec(atomic_T *atomic)
{
	__asm__ __volatile__	(	"lock	decq	%0	\n\t"
					:"=m"(atomic->value)
					:"m"(atomic->value)
					:"memory"
				);
}

__always_inline__ void atomic_set_mask(atomic_T *atomic,long mask)
{
	__asm__ __volatile__	(	"lock	orq	%1,	%0	\n\t"
					:"=m"(atomic->value)
					:"r"(mask)
					:"memory"
				);
}

__always_inline__ void atomic_clear_mask(atomic_T *atomic,long mask)
{
	__asm__ __volatile__	(	"lock	andq	%1,	%0	\n\t"
					:"=m"(atomic->value)
					:"r"(~(mask))
					:"memory"
				);
}

/**
 * @description: Atomically clear the n-th bit starting from addr
 * @param {int} nr
 * @param {volatile void} *addr
 * @return {*}
 */
static inline void clear_bit(int nr, volatile void *addr)
{
    __asm__ __volatile__("lock btrl %1, %0  \n\t"
                         : "=m"(*(volatile long *)addr)
                         : "dIr"(nr));
}

/**
 * @description: Atomically set the n-th bit starting from addr and return the previous value
 * @param {int} nr
 * @param {volatile void} *addr
 * @return {*}
 */
static inline int test_and_set_bit(int nr, volatile void *addr)
{
    int oldbit;

    __asm__ __volatile__("lock btsl %2, %1  \n\t"
                         "sbbl %0, %0  \n\t"
                         : "=r"(oldbit), "=m"(*(volatile long *)addr)
                         : "dIr"(nr)
                         : "memory");

    return oldbit;
}

/**
 * @description: Atomically clear the n-th bit starting from addr and return the previous value
 * @param {int} nr
 * @param {volatile void} *addr
 * @return {*}
 */
static inline int test_and_clear_bit(int nr, volatile void *addr)
{
    int oldbit;

    __asm__ __volatile__("lock btrl %2, %1  \n\t"
                         "sbbl %0, %0  \n\t"
                         : "=r"(oldbit), "=m"(*(volatile long *)addr)
                         : "dIr"(nr)
                         : "memory");

    return oldbit;
}

#endif
