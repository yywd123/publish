/***************************************************
*		 Copyright (c) 2018 MINE 田宇
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of version 2 of the GNU General Public
* License as published by the Free Software Foundation.
*
***************************************************/

#ifndef __STAT_H__

#define __STAT_H__

struct stat
{
	unsigned long mode;
	unsigned long file_size;
	unsigned long blocks;
};

#endif
