/***************************************************
*		 Copyright (c) 2018 MINE 田宇
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of version 2 of the GNU General Public
* License as published by the Free Software Foundation.
*
***************************************************/

#include "stat.h"
#include "VFS.h"
#include "task.h"
#include "errno.h"

unsigned long sys_fstat(int fd,struct stat *statbuf)
{
	struct file * filp = NULL;
	struct stat kstat;

//	color_printk(GREEN,BLACK,"sys_fstat:%d\n",fd);
	if(fd < 0 || fd >= TASK_FILE_MAX)
		return -EBADF;

	filp = current->file_struct[fd];
	filp->dentry->dir_inode->inode_ops->getattr(filp->dentry,(unsigned long*)&kstat);
	kstat.mode = filp->mode;

	copy_to_user(&kstat,statbuf,sizeof(struct stat));

	return 0;
}
