/*
 * @Author: your name
 * @Date: 2022-03-22 17:43:58
 * @LastEditTime: 2022-03-23 15:45:41
 * @LastEditors: Please set LastEditors
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: \kernel\tasklet.c
 */
#include "lib.h"
#include "cpu.h"
#include "spinlock.h"
#include "SMP.h"
#include "softirq.h"
#include "printk.h"
#include "tasklet.h"

struct tasklet_head tasklet_vec[NR_CPUS] = {0};    /* for regular tasklets */
struct tasklet_head tasklet_hi_vec[NR_CPUS] = {0}; /* for high-priority tasklets */

/**
 * @description: 调度 tasklet，此函数会将参数 t 指向的 tasklet 数据结构链入到 tasklet_vec 队列
 * @param {*}
 * @return {*}
 */
void tasklet_schedule(struct tasklet_struct *t)
{
    /* 检查 tasklet 的状态是否是 TASKLET_STATE_SCHED。如果是，说明 tasklet 已经被调度过了，直接返回 */
    if (!test_and_set_bit(TASKLET_STATE_SCHED, &t->state))
    {
        unsigned long flags;

        local_irq_save(flags);

        /* 将需要调度的 tasklet 加入到 per-cpu 变量 tasklet_vec 链表的表头上  */
        t->next = tasklet_vec[SMP_cpu_id()].list;
        tasklet_vec[SMP_cpu_id()].list = t;

        /* 唤起 TASKLET_SOFTIRQ 软中断，这样在下一次调用 do_softirq() 时就会执行该 tasklet */
        set_softirq_status(1UL << TASKLET_SOFTIRQ);

        local_irq_restore(flags);
    }
}

/**
 * @description: 调度 tasklet，此函数会将参数 t 指向的 tasklet 数据结构链入到 tasklet_hi_vec 队列
 * @param {*}
 * @return {*}
 */
void tasklet_hi_schedule(struct tasklet_struct *t)
{
    /* 检查 tasklet 的状态是否是 TASKLET_STATE_SCHED。如果是，说明 tasklet 已经被调度过了，直接返回 */
    if (!test_and_set_bit(TASKLET_STATE_SCHED, &t->state))
    {
        unsigned long flags;

        local_irq_save(flags);

        /* 将需要调度的 tasklet 加入到 per-cpu 变量 tasklet_hi_vec 链表的表头上  */
        t->next = tasklet_hi_vec[SMP_cpu_id()].list;
        tasklet_hi_vec[SMP_cpu_id()].list = t;

        /* 唤起 HI_SOFTIRQ 软中断，这样在下一次调用 do_softirq() 时就会执行该 tasklet */
        set_softirq_status(1UL << HI_SOFTIRQ);

        local_irq_restore(flags);
    }
}

/**
 * @description: TASKLET_SOFTIRQ 软中断处理程序
 * @param {softirq_action} *a
 * @return {*}
 */
void tasklet_action(void *a)
{
    struct tasklet_struct *list;

    /* 禁止中断（没有必要首先保存其状态，因为这里的代码总是作为软中断被调用，而且中断总是被激活的），并为当前处理器检索 tasklet_vec 链表 */
    local_irq_disable();
    list = tasklet_vec[SMP_cpu_id()].list;

    /* 将当前处理器上的 tasklet_vec 链表置为 NULL，达到清空的效果 */
    tasklet_vec[SMP_cpu_id()].list = NULL;

    /* 允许响应中断。没必要再恢复它们回原状态，因为这段程序本身就是作为软中断处理程序被调用的，所以中断是应该被允许的 */
    local_irq_enable();

    /* 循环遍历 tasklet_vec 链表上每一个待处理的 tasklet */
    while (list)
    {
        struct tasklet_struct *t = list;

        list = list->next;

        /* 如果是多处理器系统，通过检查 TASKLET_STATE_RUN 来判断这个 taskley 是否正在其它处理器上运行。
         * 如果它正在运行，那么现在就不要执行，跳到下一个待处理的 tasklet 上去。
         * 也即，同一个 tasklet 的多个实例不能在多个处理器上同时运行。
         */
        if (tasklet_trylock(t)) /* 如果当前这个 tasklet 没有执行，将其状态设置为 TASKLET_STATE_RUN，这样别的处理器就不会再去执行它了 */
        {
            /* 检查 count 值是否为 0，确保 tasklet 没有被禁止。如果 tasklet 被禁止了，则跳到下一个 pending 的 tasklet 上去 */
            if (!atomic_read(&t->count))
            {
                /* clear_bit(TASKLET_STATE_SCHED, &t->state); */
                if (!test_and_clear_bit(TASKLET_STATE_SCHED, &t->state))
                {
                    color_printk(RED, BLACK, "[tasklet_action]: tasklet state error 0x%x\n", t->state);
                }

                /* 执行 tasklet */
                t->func(t->data);

                /* tasklet 执行完毕，清除 tasklet 的 state 域的 TASKLET_STATE_RUN 状态标志 */
                tasklet_unlock(t);

                /* 重复执行下一个 pending 的tasklet，直至没有剩余的等待处理的 tasklet */
                continue;
            }

            tasklet_unlock(t);
        }

        local_irq_disable();
        t->next = tasklet_vec[SMP_cpu_id()].list;
        tasklet_vec[SMP_cpu_id()].list = t;
        set_softirq_status(1UL << TASKLET_SOFTIRQ);
        local_irq_enable();
    }
}

/**
 * @description: HI_SOFTIRQ 软中断处理程序
 * @param {softirq_action} *a
 * @return {*}
 */
void tasklet_hi_action(void *a)
{
    struct tasklet_struct *list;

    local_irq_disable();
    list = tasklet_hi_vec[SMP_cpu_id()].list;
    tasklet_hi_vec[SMP_cpu_id()].list = NULL;
    local_irq_enable();

    while (list)
    {
        struct tasklet_struct *t = list;

        list = list->next;

        if (tasklet_trylock(t))
        {
            if (!atomic_read(&t->count))
            {
                /* clear_bit(TASKLET_STATE_SCHED, &t->state); */
                if (!test_and_clear_bit(TASKLET_STATE_SCHED, &t->state))
                {
                    color_printk(RED, BLACK, "[tasklet_hi_action]: tasklet state error 0x%x\n", t->state);
                }

                t->func(t->data);
                tasklet_unlock(t);
                continue;
            }

            tasklet_unlock(t);
        }

        local_irq_disable();
        t->next = tasklet_hi_vec[SMP_cpu_id()].list;
        tasklet_hi_vec[SMP_cpu_id()].list = t;
        set_softirq_status(1UL << HI_SOFTIRQ);
        local_irq_enable();
    }
}

/**
 * @description: 初始化 tasklet
 * @param {unsigned} long
 * @param {unsigned long} data
 * @return {*}
 */
void tasklet_init(struct tasklet_struct *t, void (*func)(unsigned long), unsigned long data)
{
    t->next = NULL;
    t->state = 0;
    atomic_set(&t->count, 0);
    t->func = func;
    t->data = data;
}
