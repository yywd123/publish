/***************************************************
*		 Copyright (c) 2018 MINE 田宇
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of version 2 of the GNU General Public
* License as published by the Free Software Foundation.
*
***************************************************/

#include "softirq.h"
#include "lib.h"
#include "SMP.h"
#include "tasklet.h"

unsigned long softirq_status[NR_CPUS] = {0};
struct softirq softirq_vector[64] = {0};

void set_softirq_status(unsigned long status)
{
	softirq_status[SMP_cpu_id()] |= status;
}

unsigned long get_softirq_status()
{
	return softirq_status[SMP_cpu_id()];
}

void register_softirq(int nr,void (*action)(void * data),void * data)
{
	softirq_vector[nr].action = action;
	softirq_vector[nr].data = data;
}

void unregister_softirq(int nr)
{
	softirq_vector[nr].action = NULL;
	softirq_vector[nr].data = NULL;
}

void do_softirq()
{
	int i;
	long cpu_id = SMP_cpu_id();
	sti();
	for(i = 0;i < 64 && softirq_status[cpu_id];i++)
	{
		if(softirq_status[cpu_id] & (1 << i))
		{
			softirq_vector[i].action(softirq_vector[i].data);
			softirq_status[cpu_id] &= ~(1 << i);
		}
	}
	cli();
}

void softirq_init()
{
    memset(softirq_status, 0, sizeof(softirq_status));
	memset(softirq_vector,0,sizeof(struct softirq) * 64);

	/* 注册软中断 TASKLET_SOFTIRQ 和 HI_SOFTIRQ */
    register_softirq(TASKLET_SOFTIRQ, tasklet_action, NULL);
    register_softirq(HI_SOFTIRQ, tasklet_hi_action, NULL);
}
