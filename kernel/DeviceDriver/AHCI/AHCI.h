/***************************************************
*		 Copyright (c) 2018 MINE 田宇
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of version 2 of the GNU General Public
* License as published by the Free Software Foundation.
*
***************************************************/

#ifndef __AHCI_H__

#define __AHCI_H__

#include "block.h"

//////FIS Type value assignments
#define	AHCI_FIS_TYPE_HOST2DEVICE_FIS		0x27
#define	AHCI_FIS_TYPE_DEVICE2HOST_FIS		0x34
#define	AHCI_FIS_TYPE_DMA_ACTIVE_FIS		0x39
#define	AHCI_FIS_TYPE_DMA_SETUP_FIS		0x41
#define	AHCI_FIS_TYPE_DATA_FIS			0x46
#define	AHCI_FIS_TYPE_BIST_ACTIVE_FIS		0x58
#define	AHCI_FIS_TYPE_PIO_SETUP_FIS		0x5F
#define	AHCI_FIS_TYPE_SET_DEVICE_BIT_FIS	0xA1

struct Generic_Host_Control
{
	unsigned int CAP;	//Host Capabilities
	unsigned int GHC;	//Global Host Control
	unsigned int IS;	//Interrupt Status
	unsigned int PI;	//Ports Implemented
	unsigned int VS;	//Version
	unsigned int CCC_CTL;	//Command Completion Coalescing Control
	unsigned int CCC_PORTs;	//Command Completion Coalsecing Ports
	unsigned int EM_LOC;	//Enclosure Management Location
	unsigned int EM_CTL;	//Enclosure Management Control
	unsigned int CAP2;	//Host Capabilities Extended
	unsigned int BOHC;	//BIOS/OS Handoff Control and Status
}__attribute__((packed));


////	Generic Host Control
////Offset 00h: CAP – HBA Capabilities
#define AHCI_GHC_CAP_S64A		(1 << 31)
#define AHCI_GHC_CAP_SNCQ		(1 << 30)
#define AHCI_GHC_CAP_SSNTF		(1 << 29)
#define AHCI_GHC_CAP_SMPS		(1 << 28)
#define AHCI_GHC_CAP_SSS		(1 << 27)
#define AHCI_GHC_CAP_SALP		(1 << 26)
#define AHCI_GHC_CAP_SAL		(1 << 25)
#define AHCI_GHC_CAP_SCLO		(1 << 24)
#define AHCI_GHC_CAP_ISS(val)		((val) << 20)
#define AHCI_GHC_CAP_SAM		(1 << 18)
#define AHCI_GHC_CAP_SPM		(1 << 17)
#define AHCI_GHC_CAP_FBSS		(1 << 16)
#define AHCI_GHC_CAP_PMD		(1 << 15)
#define AHCI_GHC_CAP_SSC		(1 << 14)
#define AHCI_GHC_CAP_PSC		(1 << 13)
#define AHCI_GHC_CAP_NCS(val)		((val) << 8)
#define AHCI_GHC_CAP_CCCS		(1 << 7)
#define AHCI_GHC_CAP_EMS		(1 << 6)
#define AHCI_GHC_CAP_SXS		(1 << 5)
#define AHCI_GHC_CAP_NP(val)		((val) << 0)

////Offset 04h: GHC – Global HBA Control
#define AHCI_GHC_GHC_AE		(1 << 31)
#define AHCI_GHC_GHC_MRSM		(1 << 2)
#define AHCI_GHC_GHC_IE		(1 << 1)
#define AHCI_GHC_GHC_HR		(1 << 0)

////Offset 08h: IS – Interrupt Status Register
#define AHCI_GHC_IS_IPS(val)		((val) << 0)

////Offset 0Ch: PI – Ports Implemented
#define AHCI_GHC_PI_PI(val)		((val) << 0)

////Offset 14h: CCC_CTL – Command Completion Coalescing Control
#define AHCI_GHC_CCC_CTL_TV(val)	((val) << 16)
#define AHCI_GHC_CCC_CTL_CC(val)	((val) << 8)
#define AHCI_GHC_CCC_CTL_INT(val)	((val) << 3)
#define AHCI_GHC_CCC_CTL_EN		(1 << 0)

////Offset 18h: CCC_PORTS – Command Completion Coalescing Ports
#define AHCI_GHC_CCC_PORTS_PRT(val)	((val) << 0)

////Offset 1Ch: EM_LOC – Enclosure Management Location
#define AHCI_GHC_EM_LOC_OFST(val)	((val) << 16)
#define AHCI_GHC_EM_LOC_SZ(val)	((val) << 0)

////Offset 20h: EM_CTL – Enclosure Management Control
#define AHCI_GHC_EM_LOC_ATTR_PM	(1 << 27)
#define AHCI_GHC_EM_LOC_ATTR_ALHD	(1 << 26)
#define AHCI_GHC_EM_LOC_ATTR_XMT	(1 << 25)
#define AHCI_GHC_EM_LOC_ATTR_SMB	(1 << 24)
#define AHCI_GHC_EM_LOC_SUPP_SGPIO	(1 << 19)
#define AHCI_GHC_EM_LOC_SUPP_SES2	(1 << 18)
#define AHCI_GHC_EM_LOC_SUPP_SAFTE	(1 << 17)
#define AHCI_GHC_EM_LOC_SUPP_LED	(1 << 16)
#define AHCI_GHC_EM_LOC_CTL_RST	(1 << 9)
#define AHCI_GHC_EM_LOC_CTL_TM	(1 << 8)
#define AHCI_GHC_EM_LOC_STS_MR	(1 << 0)

////Offset 24h: CAP2 – HBA Capabilities Extended
#define AHCI_GHC_CAP2_DESO		(1 << 5)
#define AHCI_GHC_CAP2_SADM		(1 << 4)
#define AHCI_GHC_CAP2_SDS		(1 << 3)
#define AHCI_GHC_CAP2_APST		(1 << 2)
#define AHCI_GHC_CAP2_NVMP		(1 << 1)
#define AHCI_GHC_CAP2_BOH		(1 << 0)

////Offset 28h: BOHC – BIOS/OS Handoff Control and Status
#define AHCI_GHC_BOHC_BB		(1 << 4)
#define AHCI_GHC_BOHC_OOC		(1 << 3)
#define AHCI_GHC_BOHC_SOOE		(1 << 2)
#define AHCI_GHC_BOHC_OOS		(1 << 1)
#define AHCI_GHC_BOHC_BOS		(1 << 0)

struct Port_X_Control_Registers
{
	unsigned int PxCLB;		//Port x Command List Base Address
	unsigned int PxCLBU;		//Port x Command List Base Address Upper 32-Bits
	unsigned int PxFB;		//Port x FIS Base Address
	unsigned int PxFBU;		//Port x FIS Base Address Upper 32-Bits

	unsigned int PxIS;		//Port x Interrupt Status
	unsigned int PxIE;		//Port x Interrupt Enable
	unsigned int PxCMD;		//Port x Command and Status
	unsigned int Reserved0;		//Reserved

	unsigned int PxTFD;		//Port x Task File Data
	unsigned int PxSIG;		//Port x Signature
	unsigned int PxSSTS;		//Port x Serial ATA Status (SCR0: SStatus)
	unsigned int PxSCTL;		//Port x Serial ATA Control (SCR2: SControl)

	unsigned int PxSERR;		//Port x Serial ATA Error (SCR1: SError)
	unsigned int PxSACT;		//Port x Serial ATA Active (SCR3: SActive)
	unsigned int PxCI;		//Port x Command Issue
	unsigned int PxSNTF;		//Port x Serial ATA Notification (SCR4: SNotification)

	unsigned int PxFBS;		//Port x FIS-based Switching Control
	unsigned int PxDEVSLP;		//Port x Device Sleep
	unsigned int Reserved1[10];	//Reserved
	unsigned long PxVS[2];		//Port x Vendor Specific
}__attribute__((packed));

////	Port Registers (one set per port)
////Offset 00h: PxCLB – Port x Command List Base Address
#define AHCI_PORT_PxCLB_CLB(val)	((val) << 10)

////Offset 04h: PxCLBU – Port x Command List Base Address Upper 32-bits
#define AHCI_PORT_PxCLBU_CLBU(val)	((val) << 0)

////Offset 08h: PxFB – Port x FIS Base Address
#define AHCI_PORT_PxFB_FB(val)	((val) << 8)

////Offset 0Ch: PxFBU – Port x FIS Base Address Upper 32-bits
#define AHCI_PORT_PxFBU_FBU(val)	((val) << 0)

////Offset 10h: PxIS – Port x Interrupt Status
#define AHCI_PORT_PxIS_CPDS		(1 << 31)
#define AHCI_PORT_PxIS_TFES		(1 << 30)
#define AHCI_PORT_PxIS_HBFS		(1 << 29)
#define AHCI_PORT_PxIS_HBDS		(1 << 28)
#define AHCI_PORT_PxIS_IFS		(1 << 27)
#define AHCI_PORT_PxIS_INFS		(1 << 26)
#define AHCI_PORT_PxIS_OFS		(1 << 24)
#define AHCI_PORT_PxIS_IPMS		(1 << 23)
#define AHCI_PORT_PxIS_PRCS		(1 << 22)
#define AHCI_PORT_PxIS_DMPS		(1 << 7)
#define AHCI_PORT_PxIS_PCS		(1 << 6)
#define AHCI_PORT_PxIS_DPS		(1 << 5)
#define AHCI_PORT_PxIS_UFS		(1 << 4)
#define AHCI_PORT_PxIS_SDBS		(1 << 3)
#define AHCI_PORT_PxIS_DSS		(1 << 2)
#define AHCI_PORT_PxIS_PSS		(1 << 1)
#define AHCI_PORT_PxIS_DHRS		(1 << 0)

////Offset 14h: PxIE – Port x Interrupt Enable
#define AHCI_PORT_PxIE_CPDE		(1 << 31)
#define AHCI_PORT_PxIE_TFEE		(1 << 30)
#define AHCI_PORT_PxIE_HBFE		(1 << 29)
#define AHCI_PORT_PxIE_HBDE		(1 << 28)
#define AHCI_PORT_PxIE_IFE		(1 << 27)
#define AHCI_PORT_PxIE_INFE		(1 << 26)
#define AHCI_PORT_PxIE_OFE		(1 << 24)
#define AHCI_PORT_PxIE_IPME		(1 << 23)
#define AHCI_PORT_PxIE_PRCE		(1 << 22)
#define AHCI_PORT_PxIE_DMPE		(1 << 7)
#define AHCI_PORT_PxIE_PCE		(1 << 6)
#define AHCI_PORT_PxIE_DPE		(1 << 5)
#define AHCI_PORT_PxIE_UFE		(1 << 4)
#define AHCI_PORT_PxIE_SDBE		(1 << 3)
#define AHCI_PORT_PxIE_DSE		(1 << 2)
#define AHCI_PORT_PxIE_PSE		(1 << 1)
#define AHCI_PORT_PxIE_DHRE		(1 << 0)

////Offset 18h: PxCMD – Port x Command and Status
#define AHCI_PORT_PxCMD_ICC(val)	((val) << 28)
#define  AHCI_PORT_PxCMD_ICC_DEVSLEEP	(8)
#define  AHCI_PORT_PxCMD_ICC_SLUMBER	(6)
#define  AHCI_PORT_PxCMD_ICC_PARTIAL	(2)
#define  AHCI_PORT_PxCMD_ICC_ACTIVE	(1)
#define  AHCI_PORT_PxCMD_ICC_IDLE	(0)
#define AHCI_PORT_PxCMD_ASP		(1 << 27)
#define AHCI_PORT_PxCMD_ALPE		(1 << 26)
#define AHCI_PORT_PxCMD_DLAE		(1 << 25)
#define AHCI_PORT_PxCMD_ATAPI		(1 << 24)
#define AHCI_PORT_PxCMD_APSTE		(1 << 23)
#define AHCI_PORT_PxCMD_FBSCP		(1 << 22)
#define AHCI_PORT_PxCMD_ESP		(1 << 21)
#define AHCI_PORT_PxCMD_CPD		(1 << 20)
#define AHCI_PORT_PxCMD_MPSP		(1 << 19)
#define AHCI_PORT_PxCMD_HPCP		(1 << 18)
#define AHCI_PORT_PxCMD_PMA		(1 << 17)
#define AHCI_PORT_PxCMD_CPS		(1 << 16)
#define AHCI_PORT_PxCMD_CR		(1 << 15)
#define AHCI_PORT_PxCMD_FR		(1 << 14)
#define AHCI_PORT_PxCMD_MPSS		(1 << 13)
#define AHCI_PORT_PxCMD_CCS(val)	((val) << 8)
#define AHCI_PORT_PxCMD_FRE		(1 << 4)
#define AHCI_PORT_PxCMD_CLO		(1 << 3)
#define AHCI_PORT_PxCMD_POD		(1 << 2)
#define AHCI_PORT_PxCMD_SUD		(1 << 1)
#define AHCI_PORT_PxCMD_ST		(1 << 0)

////Offset 20h: PxTFD – Port x Task File Data
#define AHCI_PORT_PxTFD_ERR(val)	((val) << 8)
#define AHCI_PORT_PxTFD_STS(val)	((val) << 0)

////Offset 24h: PxSIG – Port x Signature
#define AHCI_PORT_PxSIG_SIG(val)	((val) << 0)

////Offset 28h: PxSSTS – Port x Serial ATA Status (SCR0: SStatus)
#define AHCI_PORT_PxSSTS_IPM(val)	((val) << 8)
#define AHCI_PORT_PxSSTS_SPD(val)	((val) << 4)
#define AHCI_PORT_PxSSTS_DET(val)	((val) << 0)

////Offset 2Ch: PxSCTL – Port x Serial ATA Control (SCR2: SControl)
#define AHCI_PORT_PxSCTL_IPM(val)	((val) << 8)
#define AHCI_PORT_PxSCTL_SPD(val)	((val) << 4)
#define AHCI_PORT_PxSCTL_DET(val)	((val) << 0)

////Offset 30h: PxSERR – Port x Serial ATA Error (SCR1: SError)
#define AHCI_PORT_PxSERR_DIAG_X		(1 << 26)
#define AHCI_PORT_PxSERR_DIAG_F		(1 << 25)
#define AHCI_PORT_PxSERR_DIAG_T		(1 << 24)
#define AHCI_PORT_PxSERR_DIAG_S		(1 << 23)
#define AHCI_PORT_PxSERR_DIAG_H		(1 << 22)
#define AHCI_PORT_PxSERR_DIAG_C		(1 << 21)
#define AHCI_PORT_PxSERR_DIAG_D		(1 << 20)
#define AHCI_PORT_PxSERR_DIAG_B		(1 << 19)
#define AHCI_PORT_PxSERR_DIAG_W		(1 << 18)
#define AHCI_PORT_PxSERR_DIAG_I		(1 << 17)
#define AHCI_PORT_PxSERR_DIAG_N		(1 << 16)
#define AHCI_PORT_PxSERR_ERR_E		(1 << 11)
#define AHCI_PORT_PxSERR_ERR_P		(1 << 10)
#define AHCI_PORT_PxSERR_ERR_C		(1 << 9)
#define AHCI_PORT_PxSERR_ERR_T		(1 << 8)
#define AHCI_PORT_PxSERR_ERR_M		(1 << 1)
#define AHCI_PORT_PxSERR_ERR_I		(1 << 0)

////Offset 34h: PxSACT – Port x Serial ATA Active (SCR3: SActive)
#define AHCI_PORT_PxSACT_DS(val)		((val) << 0)

////Offset 38h: PxCI – Port x Command Issue
#define AHCI_PORT_PxCI_CI(val)		((val) << 0)

////Offset 3Ch: PxSNTF – Port x Serial ATA Notification (SCR4: SNotification)
#define AHCI_PORT_PxSNTF_PMN(val)		((val) << 0)

////Offset 40h: PxFBS: Port x FIS-based Switching Control
#define AHCI_PORT_PxFBS_PMN(val)		((val) << 16)
#define AHCI_PORT_PxFBS_ADO(val)		((val) << 12)
#define AHCI_PORT_PxFBS_DEV(val)		((val) << 8)
#define AHCI_PORT_PxFBS_SDE			(1 << 2)
#define AHCI_PORT_PxFBS_DEC			(1 << 1)
#define AHCI_PORT_PxFBS_EN			(1 << 0)

////Offset 44h: PxDEVSLP – Port x Device Sleep
#define AHCI_PORT_PxDEVSLP_DM(val)		((val) << 25)
#define AHCI_PORT_PxDEVSLP_DITO(val)		((val) << 15)
#define AHCI_PORT_PxDEVSLP_MDAT(val)		((val) << 10)
#define AHCI_PORT_PxDEVSLP_DETO(val)		((val) << 2)
#define AHCI_PORT_PxDEVSLP_DSP		(1 << 1)
#define AHCI_PORT_PxDEVSLP_ADSE		(1 << 0)

struct HBA_Memory_Registers
{
	struct PCI_Header_00 *PCI_Dev;
	struct Generic_Host_Control *GHC;
	struct Port_X_Control_Registers *PxCR;
}__attribute__((packed));

/*	DMA Setup – Device to Host FIS or Host to Device FIS (bidirectional),FIS Type (41h)	*/

struct DsFIS
{
	unsigned char	FIS_Type;
	unsigned char	PM_Port:4,
			 :1,
			D:1,
			I:1,
			A:1;
	unsigned char	reserved0[2];

	unsigned long	DMA_Buffer_ID;

	unsigned int	reserved1;

	unsigned int	DMA_Buffer_Offset;

	unsigned int	DMA_Transfer_Count;

	unsigned int	reserved2;
}__attribute__((packed));

/*	PIO Setup – Device to Host FIS,FIS Type (5Fh)	*/

struct PsFIS
{
	unsigned char	FIS_Type;
	unsigned char	PM_Port:4,
			 :1,
			D:1,
			I:1,
			 :1;
	unsigned char	Status;
	unsigned char	Error;

	unsigned char	LBA0;
	unsigned char	LBA1;
	unsigned char	LBA2;
	unsigned char	Device;

	unsigned char	LBA3;
	unsigned char	LBA4;
	unsigned char	LBA5;
	unsigned char	reserved0;

	unsigned short	Count;
	unsigned char	reserved1;
	unsigned char	E_Status;

	unsigned short	Transfer_count;
	unsigned short	reserved2;	
}__attribute__((packed));

/*	Register Host to Device FIS,FIS Type (27h)	*/

struct H2DFIS
{
	unsigned char	FIS_Type;
	unsigned char	PM_Port:4,
			 :3,
			C:1;
	unsigned char	Command;
	unsigned char	Features0;

	unsigned char	LBA0;
	unsigned char	LBA1;
	unsigned char	LBA2;
	unsigned char	Device;

	unsigned char	LBA3;
	unsigned char	LBA4;
	unsigned char	LBA5;
	unsigned char	Features1;

	unsigned short	Count;
	unsigned char	ICC;
	unsigned char	Control;

	unsigned int	reserved;
}__attribute__((packed));

/*	Register Device to Host FIS,FIS Type (34h)	*/

struct D2HFIS
{
	unsigned char	FIS_Type;
	unsigned char	PM_Port:4,
			 :2,
			I:1,
			 :1;
	unsigned char	Status;
	unsigned char	Error;

	unsigned char	LBA0;
	unsigned char	LBA1;
	unsigned char	LBA2;
	unsigned char	Device;

	unsigned char	LBA3;
	unsigned char	LBA4;
	unsigned char	LBA5;
	unsigned char	reserved0;

	unsigned short	Count;
	unsigned short	reserved1;

	unsigned int	reserved2;
}__attribute__((packed));

/*	Set Device Bits - Device to Host FIS,FIS Type (A1h)	*/

struct SDBFIS
{
	unsigned char	FIS_Type;
	unsigned char	PM_Port:4,
			 :2,
			I:1,
			N:1;
	unsigned char	Status_Lo:3,
			 :1,
			Status_Hi:3,
			 :1;
	unsigned char	Error;

	unsigned int	Protocol_Specific;
}__attribute__((packed));

/*	BIST Activate FIS - bidirectional,FIS Type (58h)	*/

struct BISTFIS
{
	unsigned char	FIS_Type;
	unsigned char	PM_Port:4,
			 :4;

	unsigned char	V:1,
			 :1,
			P:1,
			F:1,
			L:1,
			S:1,
			A:1,
			T:1;
	unsigned char	reserved;

	unsigned long	Data;
}__attribute__((packed));

struct Received_FIS
{
	struct DsFIS	DMA_Setup_FIS;
	unsigned char	reserved0[4];
	struct PsFIS	PIO_Setup_FIS;
	unsigned char	reserved1[12];
	struct D2HFIS	RFIS;
	unsigned char	reserved2[4];
	struct SDBFIS	Set_Device_Bits_FIS;
	unsigned char	UFIS[64];
	unsigned char	reserved3[96];
}__attribute__((packed));

/*	Physical Region Descriptor Table	*/

struct PRDT
{
	unsigned int	DBA;
	unsigned int	DBAU;
	unsigned int	reserved;
	unsigned int	DBC:22,
			R:9,
			I:1;
}__attribute__((packed));

struct Command_Table
{
	unsigned char CMD_FIS[64];
	unsigned char CMD_APAPI[16];
	unsigned char reserved[48];
	struct PRDT CMD_PRDT[];
}__attribute__((packed));

struct Command_Table_Header
{
	unsigned int	CFL:5,
			A:1,
			W:1,
			P:1,
			R:1,
			B:1,
			C:1,
			 :1,
			PMP:4,
			PRDTL:16; 
	unsigned int	PRDBC;
	unsigned int	CTBA;
	unsigned int	CTBAU;

	unsigned int	Reserved0;
	unsigned int	Reserved1;
	unsigned int	Reserved2;
	unsigned int	Reserved3;
}__attribute__((packed));

struct Command_list
{
	struct Command_Table_Header CTBL[32];
}__attribute__((packed));

/*

*/

extern struct block_device_operation AHCI_device_operation;

void AHCI_init();

void AHCI_exit();

#endif

