/***************************************************
*		 Copyright (c) 2018 MINE 田宇
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of version 2 of the GNU General Public
* License as published by the Free Software Foundation.
*
***************************************************/

#include "module.h"
#include "printk.h"

int test_function()
{
	color_printk(BLUE,BLACK,"test_function\n");
	return 0;
}

static int test_module_init(void)
{
	color_printk(RED,BLACK,"test_module_init\n");
	test_function();
	return 0;
}

static void test_module_exit(void)
{
	color_printk(RED,BLACK,"test_module_exit\n");
}

module_init(test_module_init);
module_exit(test_module_exit);

