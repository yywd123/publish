/***************************************************
*		 Copyright (c) 2018 MINE 田宇
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of version 2 of the GNU General Public
* License as published by the Free Software Foundation.
*
***************************************************/

#include "GPU.h"
#include "GPUID.h"
#include "printk.h"
#include "../PCIe/PCI.h"
#include "../APIC/APIC.h"
#include "memory.h"

struct PCI_Header_00 GPU_PCI_HBA;
struct PCI_Header_00 PCH_PCI_HBA;

unsigned char *GTTMMADR,*GTTADR,*GMADR;
unsigned long GTTMMSIZE,GTTSIZE,GMSIZE;
unsigned char *DSMADR;
unsigned long DSMSIZE;
unsigned char *GPIO_MMIO_ADR;
unsigned char *ASLSADR;
unsigned char *MCBARADR;
unsigned char *VBTADR;
unsigned char *ACPI_CLID;
unsigned int  MBoxes;

unsigned long GPU_install(unsigned long irq,void * arg)
{
	return 0;
}

void GPU_enable(unsigned long irq)
{
}

hw_int_controller GPU_int_controller = 
{
	.enable = GPU_enable,
	.disable = NULL,
	.install = GPU_install,
	.uninstall = NULL,
	.ack = Local_APIC_edge_level_ack,
};

void GPU_handler(unsigned long nr, unsigned long parameter, struct pt_regs * regs);

void init_GPU(void)
{
	int bus,device,function;
	unsigned int index = 0;
	unsigned int value = 0;
	unsigned long * tmp = NULL;
	unsigned int * ptr = NULL;
	unsigned char *start,*end;
	int i;
	unsigned int flags;

	unsigned int stolen_size;
	unsigned int gtt_size;
	unsigned int gtt_total;

	color_printk(YELLOW,BLACK,"-------------------------------------------------------\n");

	for(bus = 0;bus <= 255;bus++)
		for(device = 0;device <= 31;device++)
			for(function = 0;function <= 7;function++)
			{
				value = Read_PCI_Config(bus,device,function,0);
				if((value & 0xffff) != 0x8086)	//PCH_PCI_HBA.VendorID
					continue;
				value = Read_PCI_Config(bus,device,function,8);
				if(((value >> 24) & 0xff) != 0x06)	//PCH_PCI_HBA.ClassCode
					continue;
				if(((value >> 16) & 0xff) != 0x01)	//PCH_PCI_HBA.SubClass
					continue;

				analysis_PCI_Config(&PCH_PCI_HBA,bus,device,function);
				color_printk(YELLOW,BLACK,"Intel PCH VendorID:%#06x,DeviceID:%#06x,ClassCode:%#06x,SubClass:%#06x\n", PCH_PCI_HBA.VendorID, PCH_PCI_HBA.DeviceID, PCH_PCI_HBA.ClassCode, PCH_PCI_HBA.SubClass);

				goto analysis_intel_PCH;
			}
analysis_intel_PCH:
	switch(PCH_PCI_HBA.DeviceID & 0xff00)
	{
		case 0x3b00:
			color_printk(YELLOW,BLACK,"Found Ibex Peak PCH\n");
			break;
		case 0x1c00:
			color_printk(YELLOW,BLACK,"Found CougarPoint PCH\n");
			break;
		case 0x1e00:
			color_printk(YELLOW,BLACK,"Found PantherPoint PCH\n");
			break;
		case 0x8c00:
			color_printk(YELLOW,BLACK,"Found LynxPoint PCH\n");
			break;
		case 0x9c00:
			color_printk(YELLOW,BLACK,"Found LynxPoint LP PCH\n");
			break;
		case 0x8c80:
			color_printk(YELLOW,BLACK,"Found WildcatPoint PCH\n");
			break;
		case 0x9c80:
			color_printk(YELLOW,BLACK,"Found WildcatPoint LP PCH\n");
			break;
		case 0xa100:
			color_printk(YELLOW,BLACK,"Found SunrisePoint PCH\n");
			break;
		case 0x9d00:
			color_printk(YELLOW,BLACK,"Found SunrisePoint LP PCH\n");
			break;
		case 0xa280:
			color_printk(YELLOW,BLACK,"Found Kaby Lake PCH (KBP)\n");
			break;
		case 0xa300:
			color_printk(YELLOW,BLACK,"Found Cannon Lake PCH (CNP)\n");
			break;
		case 0x9d80:
			color_printk(YELLOW,BLACK,"Found Cannon Lake LP PCH (CNP-LP)\n");
			break;
		case 0x0280:
		case 0x0680:
			color_printk(YELLOW,BLACK,"Found Comet Lake PCH (CMP)\n");
			break;
		case 0xa380:
			color_printk(YELLOW,BLACK,"Found Comet Lake V PCH (CMP-V)\n");
			break;
		case 0x3480:
			color_printk(YELLOW,BLACK,"Found Ice Lake PCHn");
			break;
		case 0x4b00:
			color_printk(YELLOW,BLACK,"Found Mule Creek Canyon PCH\n");
			break;
		case 0xa080:
		case 0x4380:
			color_printk(YELLOW,BLACK,"Found Tiger Lake LP PCH\n");
			break;
		case 0x4d80:
		case 0x3880:
			color_printk(YELLOW,BLACK,"Found Jasper Lake PCH\n");
			break;
		default:
			break; 
	}

	////0x48
	value = Read_PCI_Config(bus,device,function,0x48);
	color_printk(WHITE,BLACK,"Memory Base Address(MBA):%#018lx\n",(unsigned long)Read_PCI_Config(bus,device,function,0x4c) << 32 | value);

	color_printk(YELLOW,BLACK,"-------------------------------------------------------\n");

	for(bus = 0;bus <= 255;bus++)
		for(device = 0;device <= 31;device++)
			for(function = 0;function <= 7;function++)
			{
				value = Read_PCI_Config(bus,device,function,0);
				if((value & 0xffff) != 0x8086)	//GPU_PCI_HBA.VendorID
					continue;
				value = Read_PCI_Config(bus,device,function,8);
				if(((value >> 24) & 0xff) != 0x03)	//GPU_PCI_HBA.ClassCode
					continue;
				if(((value >> 16) & 0xff) != 0x00)	//GPU_PCI_HBA.SubClass
					continue;

				color_printk(YELLOW,BLACK,"-------------------------------------------------------\n");

				analysis_PCI_Config(&GPU_PCI_HBA,bus,device,function);

				for(i = 0;i < sizeof(GPU_id_list)/sizeof(struct pci_device_id);i++)
					if((GPU_id_list[i].VendorID == GPU_PCI_HBA.VendorID) && (GPU_id_list[i].DeviceID == GPU_PCI_HBA.DeviceID))
					{
						color_printk(YELLOW,BLACK,"Intel GPU %s gen=%d VendorID:%#06x,DeviceID:%#06x\n", GPU_id_list[i].name, GPU_id_list[i].platform, GPU_id_list[i].VendorID, GPU_id_list[i].DeviceID);
						goto analysis_intel_GPU;
					}
			}

analysis_intel_GPU:

	color_printk(YELLOW,BLACK,"-------------------------------------------------------\n");	

	////0x00
	value = Read_PCI_Config(bus,device,function,0x0);
	color_printk(WHITE,BLACK,"Vendor Identification Number(VID):%#06x\n",value & 0xffff);
	////0x02
	color_printk(WHITE,BLACK,"Device Identification Number-LSB(DID_LSB):%#06x\n",value >> 16 & 0x3);
	color_printk(WHITE,BLACK,"Device Identification Number-SKU(DID_SKU):%#06x\n",value >> 18 & 0x3);
	color_printk(WHITE,BLACK,"Device Identification Number-MSB(DID_MSB):%#06x\n",value >> 20 & 0xfff);

	////0x04
	value = Read_PCI_Config(bus,device,function,0x4);
	color_printk(WHITE,BLACK,"I/O Access Enable(IOAE):%#06x\n",value & 0x1);
	color_printk(WHITE,BLACK,"Bus Master Enable(BME):%#06x\n",value >> 2 & 0x1);
	color_printk(WHITE,BLACK,"Special Cycle Enable(SCE):%#06x\n",value >> 3 & 0x1);
	color_printk(WHITE,BLACK,"Memory Write and Invalidate Enable(MWIE):%#06x\n",value >> 4 & 0x1);
	color_printk(WHITE,BLACK,"Video Palette Snooping(VPS):%#06x\n",value >> 5 & 0x1);
	color_printk(WHITE,BLACK,"Parity Error Enable(PER):%#06x\n",value >> 6 & 0x1);
	color_printk(WHITE,BLACK,"Wait Cycle Control(WCC):%#06x\n",value >> 7 & 0x1);
	color_printk(WHITE,BLACK,"SERR Enable(SEN):%#06x\n",value >> 8 & 0x1);
	color_printk(WHITE,BLACK,"Fast Back-to-Back(FB2B):%#06x\n",value >> 9 & 0x1);
	color_printk(WHITE,BLACK,"Interrupt Disable(INTDIS):%#06x\n",value >> 10 & 0x1);
	////0x06
	color_printk(WHITE,BLACK,"Interrupt Status(INTSTS):%#06x\n",value >> 19 & 0x1);
	color_printk(WHITE,BLACK,"Capability List(CLIST):%#06x\n",value >> 20 & 0x1);
	color_printk(WHITE,BLACK,"66 MHz PCI Capable(C66):%#06x\n",value >> 21 & 0x1);
	color_printk(WHITE,BLACK,"User Defined Format(UDF):%#06x\n",value >> 22 & 0x1);
	color_printk(WHITE,BLACK,"Fast Back-to-Back(FB2B):%#06x\n",value >> 23 & 0x1);
	color_printk(WHITE,BLACK,"Master Data Parity Error Detected(DPD):%#06x\n",value >> 24 & 0x1);
	color_printk(WHITE,BLACK,"DEVSEL Timing(DEVT):%#06x\n",value >> 25 & 0x3);
	color_printk(WHITE,BLACK,"Signaled Target Abort Status(STAS):%#06x\n",value >> 27 & 0x1);
	color_printk(WHITE,BLACK,"Received Target Abort Status(RTAS):%#06x\n",value >> 28 & 0x1);
	color_printk(WHITE,BLACK,"Received Master Abort Status(RMAS):%#06x\n",value >> 29 & 0x1);
	color_printk(WHITE,BLACK,"Signaled System Error(SSE):%#06x\n",value >> 30 & 0x1);
	color_printk(WHITE,BLACK,"Detected Parity Error(DPE):%#06x\n",value >> 31 & 0x1);

	////0x08
	value = Read_PCI_Config(bus,device,function,0x8);
	color_printk(WHITE,BLACK,"Revision Identification Number(RID):%#06x\n",value & 0xf);
	color_printk(WHITE,BLACK,"Revision Identification Number MSB(RID_MSB):%#06x\n",value >> 16 & 0xf);
	////0x09
	color_printk(WHITE,BLACK,"Programming Interface(PI):%#06x\n",value >> 8 & 0xff);
	color_printk(WHITE,BLACK,"Sub-Class Code(SUBCC):%#06x\n",value >> 16 & 0xff);
	color_printk(WHITE,BLACK,"Base Class Code(BCC):%#06x\n",value >> 23 & 0xff);

	////0x0c
	value = Read_PCI_Config(bus,device,function,0xc);
	color_printk(WHITE,BLACK,"Cache Line Size(CLS):%#06x\n",value & 0xff);
	////0x0d
	color_printk(WHITE,BLACK,"Master Latency Timer Count Value(MLTCV):%#06x\n",value >> 8 & 0xff);
	////0x0e
	color_printk(WHITE,BLACK,"Header Code(H):%#06x\n",value >> 16 & 0x7f);
	color_printk(WHITE,BLACK,"Multi Function Status(MFUNC):%#06x\n",value >> 23 & 0x1);
	////0x0f
	color_printk(WHITE,BLACK,"BIST Supported(BISTS):%#06x\n",value >> 31 & 0x1);

	////0x10
	value = Read_PCI_Config(bus,device,function,0x10);
	color_printk(WHITE,BLACK,"Memory Type(MEMTYP):%#06x\n",value >> 1 & 0x3);
	color_printk(WHITE,BLACK,"Prefetchable Memory(PREFMEM):%#06x\n",value >> 3 & 0x1);
	color_printk(WHITE,BLACK,"Address Mask(ADMSK):%#010x\n",value >> 4 & 0x3ffff);
	color_printk(WHITE,BLACK,"Memory Base Address(MBA):%#018lx\n",((unsigned long)Read_PCI_Config(bus,device,function,0x14) << 32 | value) & 0x7fffc00000);
	color_printk(WHITE,BLACK,"Reserved for Memory Base Address(RSVDRW):%#018lx\n",((unsigned long)Read_PCI_Config(bus,device,function,0x14) << 32 | value) & 0xffffff8000000000);

	////0x18
	value = Read_PCI_Config(bus,device,function,0x18);
	color_printk(WHITE,BLACK,"Memory Type(MEMTYP):%#06x\n",value >> 1 & 0x3);
	color_printk(WHITE,BLACK,"Prefetchable Memory(PREFMEM):%#06x\n",value >> 3 & 0x1);
	color_printk(WHITE,BLACK,"Address Mask(ADMSK):%#010x\n",value >> 4 & 0x3ffff);
	color_printk(WHITE,BLACK,"256 MB Address Mask(ADMSK256):%#010x\n",value >> 27 & 0x1);
	color_printk(WHITE,BLACK,"512 MB Address Mask(ADMSK512):%#010x\n",value >> 28 & 0x1);
	color_printk(WHITE,BLACK,"Memory Base Address(MBA):%#018lx\n",((unsigned long)Read_PCI_Config(bus,device,function,0x1c) << 32| value) & 0x7fe0000000);
	color_printk(WHITE,BLACK,"Reserved for Memory Base Address(RSVDRW):%#018lx\n",((unsigned long)Read_PCI_Config(bus,device,function,0x1c) << 32 | value) & 0xffffff8000000000);

	////0x20
	value = Read_PCI_Config(bus,device,function,0x20);
	color_printk(WHITE,BLACK,"Memory/IO Space(MIOS):%#06x\n",value & 0x1);
	color_printk(WHITE,BLACK,"Memory Type(MEMTYPE):%#06x\n",value >> 1 & 0x3);
	color_printk(WHITE,BLACK,"IO Base Address(IOBASE):%#06x\n",value & 0xffc0);

	////0x2c
	value = Read_PCI_Config(bus,device,function,0x2c);
	color_printk(WHITE,BLACK,"Subsystem Vendor ID(SUBVID):%#06x\n",value & 0xffff);
	////0x2e
	color_printk(WHITE,BLACK,"Subsystem Identification(SUBID):%#06x\n",value >> 16 & 0xffff);

	////0x30
	value = Read_PCI_Config(bus,device,function,0x30);
	color_printk(WHITE,BLACK,"ROM BIOS Enable(RBE):%#06x\n",value & 0x1);
	color_printk(WHITE,BLACK,"Address Mask(ADMSK):%#06x\n",value >> 11 & 0x7f);
	color_printk(WHITE,BLACK,"ROM Base Address(RBA):%#06x\n",value & 0xfffc0000);

	////0x34
	value = Read_PCI_Config(bus,device,function,0x34);
	color_printk(WHITE,BLACK,"Capabilities Pointer Value(CPV):%#06x\n",value & 0xff);

	////0x3c
	value = Read_PCI_Config(bus,device,function,0x3c);
	color_printk(WHITE,BLACK,"Interrupt Connection(INTCON):%#06x\n",value & 0xff);
	////0x3d
	color_printk(WHITE,BLACK,"Interrupt Pin(INTPIN):%#06x\n",value >> 8 & 0xff);
	////0x3e
	color_printk(WHITE,BLACK,"Minimum Grant Value(MGV):%#06x\n",value >> 16 & 0xff);
	////0x3f
	color_printk(WHITE,BLACK,"Maximum Latency Value(MLV):%#06x\n",value >> 24 & 0xff);

	////0x40
	value = Read_PCI_Config(bus,device,function,0x40);
	color_printk(WHITE,BLACK,"Capability Identifier(CAP_ID):%#06x\n",value & 0xff);
	color_printk(WHITE,BLACK,"Next Capability Pointer(NEXT_CAP):%#06x\n",value >> 8 & 0xff);
	////0x42
	color_printk(WHITE,BLACK,"CAPID Length(CAPIDLEN):%#06x\n",value >> 16 & 0xff);
	color_printk(WHITE,BLACK,"CAPID Version(CAPID_VER):%#06x\n",value >> 24 & 0xf);

	////0x44
	value = Read_PCI_Config(bus,device,function,0x44);
	color_printk(WHITE,BLACK,"DDR3L_EN:%#06x\n",value & 0x1);
	color_printk(WHITE,BLACK,"DDR_WRTVREF:%#06x\n",value >> 1 & 0x1);
	color_printk(WHITE,BLACK,"OC_ENABLED_DSKU:%#06x\n",value >> 2 & 0x1);
	color_printk(WHITE,BLACK,"DDR_OVERCLOCK:%#06x\n",value >> 3 & 0x1);
	color_printk(WHITE,BLACK,"CRID:%#06x\n",value >> 4 & 0xf);
	color_printk(WHITE,BLACK,"CDID:%#06x\n",value >> 8 & 0x3);
	color_printk(WHITE,BLACK,"DIDOE:%#06x\n",value >> 10 & 0x1);
	color_printk(WHITE,BLACK,"IGD:%#06x\n",value >> 11 & 0x1);
	color_printk(WHITE,BLACK,"PDCD:%#06x\n",value >> 12 & 0x1);
	color_printk(WHITE,BLACK,"X2APIC_EN:%#06x\n",value >> 13 & 0x1);
	color_printk(WHITE,BLACK,"DDPCD:%#06x\n",value >> 14 & 0x1);
	color_printk(WHITE,BLACK,"CDD:%#06x\n",value >> 15 & 0x1);
	color_printk(WHITE,BLACK,"FUFRD:%#06x\n",value >> 16 & 0x1);
	color_printk(WHITE,BLACK,"D1NM:%#06x\n",value >> 17 & 0x1);
	color_printk(WHITE,BLACK,"PCIE_RATIO_DIS:%#06x\n",value >> 18 & 0x1);
	color_printk(WHITE,BLACK,"DDRSZ:%#06x\n",value >> 19 & 0x3);
	color_printk(WHITE,BLACK,"PEGG2DIS:%#06x\n",value >> 21 & 0x1);
	color_printk(WHITE,BLACK,"DMIG2DIS:%#06x\n",value >> 22 & 0x1);
	color_printk(WHITE,BLACK,"VTDD:%#06x\n",value >> 23 & 0x1);
	color_printk(WHITE,BLACK,"FDEE:%#06x\n",value >> 24 & 0x1);
	color_printk(WHITE,BLACK,"ECCDIS:%#06x\n",value >> 25 & 0x1);
	color_printk(WHITE,BLACK,"DW:%#06x\n",value >> 26 & 0x1);
	color_printk(WHITE,BLACK,"PELWUD:%#06x\n",value >> 27 & 0x1);
	color_printk(WHITE,BLACK,"PEG10D:%#06x\n",value >> 28 & 0x1);
	color_printk(WHITE,BLACK,"PEG11D:%#06x\n",value >> 29 & 0x1);
	color_printk(WHITE,BLACK,"PEG12D:%#06x\n",value >> 30 & 0x1);
	color_printk(WHITE,BLACK,"DHDAD:%#06x\n",value >> 31 & 0x1);

	////0x48
	value = Read_PCI_Config(bus,device,function,0x48);
	color_printk(WHITE,BLACK,"SPEGFX1:%#06x\n",value & 0x1);
	color_printk(WHITE,BLACK,"DPEGFX1:%#06x\n",value >> 1 & 0x1);
	color_printk(WHITE,BLACK,"SPARE2:%#06x\n",value >> 2 & 0x1);
	color_printk(WHITE,BLACK,"SPARE3:%#06x\n",value >> 3 & 0x1);
	color_printk(WHITE,BLACK,"DMFC:%#06x\n",value >> 4 & 0x7);
	color_printk(WHITE,BLACK,"DDD:%#06x\n",value >> 7 & 0x1);
	color_printk(WHITE,BLACK,"SPARE10_8:%#06x\n",value >> 8 & 0x7);
	color_printk(WHITE,BLACK,"SPARE15_12:%#06x\n",value >> 12 & 0xf);
	color_printk(WHITE,BLACK,"PEGX16D:%#06x\n",value >> 16 & 0x1);
	color_printk(WHITE,BLACK,"ADDGFXCAP:%#06x\n",value >> 17 & 0x1);
	color_printk(WHITE,BLACK,"ADDGFXEN:%#06x\n",value >> 18 & 0x1);
	color_printk(WHITE,BLACK,"PKGTYP:%#06x\n",value >> 19 & 0x3);
	color_printk(WHITE,BLACK,"PEGG3_DIS:%#06x\n",value >> 20 & 0x1);
	color_printk(WHITE,BLACK,"PLL_REF100_CFG:%#06x\n",value >> 21 & 0x7);
	color_printk(WHITE,BLACK,"SOFTBIN:%#06x\n",value >> 24 & 0x1);
	color_printk(WHITE,BLACK,"CACHESZ:%#06x\n",value >> 25 & 0x7);
	color_printk(WHITE,BLACK,"SMT:%#06x\n",value >> 28 & 0x1);
	color_printk(WHITE,BLACK,"OC_ENABLED:%#06x\n",value >> 29 & 0x1);
	color_printk(WHITE,BLACK,"OC_CTL_DSKU_DIS:%#06x\n",value >> 30 & 0x1);
	color_printk(WHITE,BLACK,"SPARE31:%#06x\n",value >> 31 & 0x1);

	////0x50
	value = Read_PCI_Config(bus,device,function,0x50);
	color_printk(WHITE,BLACK,"GGC Lock(GGCLCK):%#06x\n",value & 0x1);
	color_printk(WHITE,BLACK,"IGD VGA Disable(IVD):%#06x\n",value >> 1 & 0x1);
	color_printk(WHITE,BLACK,"Graphics Mode Select(GMS):%#06x\n",value >> 3 & 0x1f);
	color_printk(WHITE,BLACK,"GTT Graphics Memory Size(GGMS):%#06x\n",value >> 8 & 0x3);
	color_printk(WHITE,BLACK,"Versatile Acceleration Mode Enable(VAMEN):%#06x\n",value >> 14 & 0x1);

	////0x54
	value = Read_PCI_Config(bus,device,function,0x54);
	color_printk(WHITE,BLACK,"D0EN:%#06x\n",value & 0x1);
	color_printk(WHITE,BLACK,"D1F2EN:%#06x\n",value >> 1 & 0x1);
	color_printk(WHITE,BLACK,"D1F1EN:%#06x\n",value >> 2 & 0x1);
	color_printk(WHITE,BLACK,"D1F0EN:%#06x\n",value >> 3 & 0x1);
	color_printk(WHITE,BLACK,"D2EN:%#06x\n",value >> 4 & 0x1);
	color_printk(WHITE,BLACK,"D3EN:%#06x\n",value >> 5 & 0x1);
	color_printk(WHITE,BLACK,"D4EN:%#06x\n",value >> 7 & 0x1);
	color_printk(WHITE,BLACK,"D7EN:%#06x\n",value >> 14 & 0x1);

	////0x5c
	value = Read_PCI_Config(bus,device,function,0x5c);
	color_printk(WHITE,BLACK,"Lock(LOCK):%#06x\n",value & 0x1);
	color_printk(WHITE,BLACK,"Graphics Base of Stolen Memory (BDSM):%#010x\n",value & 0xfff00000);

	////0x60
	value = Read_PCI_Config(bus,device,function,0x60);
	color_printk(WHITE,BLACK,"Reserved R/W(ReservedRW):%#06x\n",value & 0xffff);
	////0x62
	color_printk(WHITE,BLACK,"Untrusted Aperture Size Low(LHSASL):%#06x\n",value >> 17 & 0x1);
	color_printk(WHITE,BLACK,"Untrusted Aperture Size High(LHSASH):%#06x\n",value >> 18 & 0x1);
	color_printk(WHITE,BLACK,"Reserved R/W(RSVDRW):%#06x\n",value >> 20 & 0xf);

	////0x90
	value = Read_PCI_Config(bus,device,function,0x90);
	color_printk(WHITE,BLACK,"Capability ID(CAPID):%#06x\n",value & 0xff);
	color_printk(WHITE,BLACK,"Pointer to Next Capability(POINTNEXT):%#06x\n",value >> 8 & 0xff);
	////0x92
	color_printk(WHITE,BLACK,"MSI Enable(MSIEN):%#06x\n",value >> 16 & 0x1);
	color_printk(WHITE,BLACK,"Multiple Message Capable(MMC):%#06x\n",value >> 17 & 0x7);
	color_printk(WHITE,BLACK,"Multiple Message Enable(MME):%#06x\n",value >> 20 & 0x7);
	color_printk(WHITE,BLACK,"64 Bit Capable(CAP64B):%#06x\n",value >> 23 & 0x1);

	////0x94
	value = Read_PCI_Config(bus,device,function,0x94);
	color_printk(WHITE,BLACK,"Force Dword Align(FDWORD):%#06x\n",value & 0x3);
	color_printk(WHITE,BLACK,"Message Address(MESSADD):%#010x\n",value & 0xfffffffc);

	////0x98
	value = Read_PCI_Config(bus,device,function,0x98);
	color_printk(WHITE,BLACK,"Message Data(MESSDATA):%#06x\n",value & 0xffff);

	////0xa4
	value = Read_PCI_Config(bus,device,function,0xa4);
	color_printk(WHITE,BLACK,"CAP_ID:%#06x\n",value & 0xff);
	color_printk(WHITE,BLACK,"NEXT_PTR:%#06x\n",value >> 8 & 0xff);
	////0xa6
	color_printk(WHITE,BLACK,"Capability Length(CAP_LEN):%#06x\n",value >> 16 & 0xff);
	color_printk(WHITE,BLACK,"TXP Capability(TXP_CAP):%#06x\n",value >> 24 & 0x1);
	color_printk(WHITE,BLACK,"FLR Capability(FLR_CAP):%#06x\n",value >> 25 & 0x1);

	////0xa8
	value = Read_PCI_Config(bus,device,function,0xa8);
	color_printk(WHITE,BLACK,"Initiate Function Level Reset(INIT_FLR):%#06x\n",value & 0x1);
	////0xa9
	color_printk(WHITE,BLACK,"Transactions Pending(TP):%#06x\n",value >> 16 & 0x1);

	////0xd0
	value = Read_PCI_Config(bus,device,function,0xd0);
	color_printk(WHITE,BLACK,"Capability Identifier(CAP_ID):%#06x\n",value & 0xff);
	color_printk(WHITE,BLACK,"Next Capability Pointer(NEXT_PTR):%#06x\n",value >> 8 & 0xff);
	////0xd2
	color_printk(WHITE,BLACK,"Version(VER):%#06x\n",value >> 16 & 0x7);
	color_printk(WHITE,BLACK,"PME Clock(PMECLK):%#06x\n",value >> 19 & 0x1);
	color_printk(WHITE,BLACK,"Device Specific Initialization(DSI):%#06x\n",value >> 21 & 0x1);
	color_printk(WHITE,BLACK,"D1 Support(D1):%#06x\n",value >> 25 & 0x1);
	color_printk(WHITE,BLACK,"D2 Support(D2):%#06x\n",value >> 26 & 0x1);
	color_printk(WHITE,BLACK,"PME Support(PMES):%#06x\n",value >> 27 & 0x1f);

	////0xd4
	value = Read_PCI_Config(bus,device,function,0xd4);
	color_printk(WHITE,BLACK,"PWRSTAT:%#06x\n",value & 0x3);
	color_printk(WHITE,BLACK,"PMEEN:%#06x\n",value >> 8 & 0x1);
	color_printk(WHITE,BLACK,"DSEL:%#06x\n",value >> 9 & 0xf);
	color_printk(WHITE,BLACK,"DSCALE:%#06x\n",value >> 13 & 0x3);
	color_printk(WHITE,BLACK,"PMESTS:%#06x\n",value >> 15 & 0x1);

	////0xe0
	value = Read_PCI_Config(bus,device,function,0xe0);
	color_printk(WHITE,BLACK,"GMCH Software SMI Event(GSSMIE):%#06x\n",value & 0x1);
	color_printk(WHITE,BLACK,"Software Flag(SWF):%#06x\n",value >> 1 & 0x7f);
	color_printk(WHITE,BLACK,"Software Scratch Bits(SWSB):%#06x\n",value >> 8 & 0xff);

	////0xe4
	value = Read_PCI_Config(bus,device,function,0xe4);
	color_printk(WHITE,BLACK,"GSE Scratch Trigger 0(GSE0):%#06x\n",value & 0xff);
	color_printk(WHITE,BLACK,"GSE Scratch Trigger 1(GSE1):%#06x\n",value >> 8 & 0xff);
	color_printk(WHITE,BLACK,"GSE Scratch Trigger 2(GSE2):%#06x\n",value >> 16 & 0xff);
	color_printk(WHITE,BLACK,"GSE Scratch Trigger 3(GSE3):%#06x\n",value >> 24 & 0xff);

	////0xe8
	value = Read_PCI_Config(bus,device,function,0xe8);
	color_printk(WHITE,BLACK,"Software SCI Event(GSSCIE):%#06x\n",value & 0x1);
	color_printk(WHITE,BLACK,"Software scratch bits(SCISB):%#06x\n",value >> 1 & 0x3fff);
	color_printk(WHITE,BLACK,"SMI or SCI event select(SMISCISEL):%#06x\n",value >> 15 & 0x1);

	////0xfc
	value = Read_PCI_Config(bus,device,function,0xfc);
	color_printk(WHITE,BLACK,"Device Switching Storage(DSS):%#010x\n",value);

	color_printk(YELLOW,BLACK,"-------------------------------------------------------\n");	
	color_printk(BLUE,BLACK,"map BAR0 space to page table\n");

	for(start = (unsigned char *)(unsigned long)GPU_PCI_HBA.Base32Address0,end = (unsigned char *)(unsigned long)(GPU_PCI_HBA.Base32Address0 + GPU_PCI_HBA.Base32Limit0);start < end;start += PAGE_2M_SHIFT)
	{
		tmp = Phy_To_Virt(Get_gdt() + (((unsigned long)Phy_To_Virt(start) >> PAGE_GDT_SHIFT) & 0x1ff));
		if (*tmp == 0)
		{
		unsigned long * virtual = kmalloc(PAGE_4K_SIZE,0);
		memset(virtual,0,PAGE_4K_SIZE);
		set_mpl4t(tmp,mk_mpl4t(Virt_To_Phy(virtual),PAGE_KERNEL_GDT));
		}

		tmp = Phy_To_Virt((unsigned long *)(*tmp & (~ 0xfffUL)) + (((unsigned long)Phy_To_Virt(start) >> PAGE_1G_SHIFT) & 0x1ff));
		if(*tmp == 0)
		{
		unsigned long * virtual = kmalloc(PAGE_4K_SIZE,0);
		memset(virtual,0,PAGE_4K_SIZE);
		set_pdpt(tmp,mk_pdpt(Virt_To_Phy(virtual),PAGE_KERNEL_Dir));
		}
	
		tmp = Phy_To_Virt((unsigned long *)(*tmp & (~ 0xfffUL)) + (((unsigned long)Phy_To_Virt(start) >> PAGE_2M_SHIFT) & 0x1ff));
		set_pdt(tmp,mk_pdt((unsigned long)start & PAGE_2M_MASK,PAGE_KERNEL_Page | PAGE_PWT | PAGE_PCD));	
	}

	color_printk(BLUE,BLACK,"map BAR2 space to page table\n");

	for(start = (unsigned char *)(unsigned long)GPU_PCI_HBA.Base32Address2,end = (unsigned char *)(unsigned long)(GPU_PCI_HBA.Base32Address2 + GPU_PCI_HBA.Base32Limit2);start < end;start += PAGE_2M_SHIFT)
	{
		tmp = Phy_To_Virt(Get_gdt() + (((unsigned long)Phy_To_Virt(start) >> PAGE_GDT_SHIFT) & 0x1ff));
		if (*tmp == 0)
		{
		unsigned long * virtual = kmalloc(PAGE_4K_SIZE,0);
		memset(virtual,0,PAGE_4K_SIZE);
		set_mpl4t(tmp,mk_mpl4t(Virt_To_Phy(virtual),PAGE_KERNEL_GDT));
		}

		tmp = Phy_To_Virt((unsigned long *)(*tmp & (~ 0xfffUL)) + (((unsigned long)Phy_To_Virt(start) >> PAGE_1G_SHIFT) & 0x1ff));
		if(*tmp == 0)
		{
		unsigned long * virtual = kmalloc(PAGE_4K_SIZE,0);
		memset(virtual,0,PAGE_4K_SIZE);
		set_pdpt(tmp,mk_pdpt(Virt_To_Phy(virtual),PAGE_KERNEL_Dir));
		}
	
		tmp = Phy_To_Virt((unsigned long *)(*tmp & (~ 0xfffUL)) + (((unsigned long)Phy_To_Virt(start) >> PAGE_2M_SHIFT) & 0x1ff));
		set_pdt(tmp,mk_pdt((unsigned long)start & PAGE_2M_MASK,PAGE_KERNEL_Page | PAGE_PWT | PAGE_PCD));	
	}

	flush_tlb();

	color_printk(YELLOW,BLACK,"-------------------------------------------------------\n");
while(1);	//##############################################################################################################
}


void GPU_handler(unsigned long nr, unsigned long parameter, struct pt_regs * regs)
{
	unsigned int DE_IER,SDE_IER;
	unsigned int GT_IIR,DE_IIR,PM_IIR;
	color_printk(BLACK,WHITE,"GPU_handler\n");

	/* disable master interrupt before clearing iir  */
	DE_IER = intel_uncore_read32(GTTMMADR,0x4400c);			//DEIER
	intel_uncore_write32(GTTMMADR,0x4400c,DE_IER & ~(1 << 31));
	intel_uncore_read32(GTTMMADR,0x4400c);

	SDE_IER = intel_uncore_read32(GTTMMADR,0xc400c);		//SDEIER
	intel_uncore_write32(GTTMMADR,0xc400c,0);
	intel_uncore_read32(GTTMMADR,0xc400c);

	GT_IIR = intel_uncore_read32(GTTMMADR,0x44018);			//GTIIR
	if(GT_IIR)
	{
		intel_uncore_write32(GTTMMADR,0x44018,GT_IIR);
		//GT irq handler
		if(GT_IIR & GT_RENDER_COMMAND_PARSER_USER_INTERRUPT)				//0
		{
			color_printk(RED,GREEN,"[GT ERROR]");
			color_printk(RED,BLACK,"Render Command Parser User Interrupt:%#010x\n",GT_IIR);	
		}

		if(GT_IIR & GT_RENDER_DEBUG_INTERRUPT)						//1
		{
			color_printk(RED,GREEN,"[GT ERROR]");
			color_printk(RED,BLACK,"Render Debug Interrupt:%#010x\n",GT_IIR);	
		}

		if(GT_IIR & GT_RENDER_MMIO_SYNC_FLUSH_STATUS)					//2
		{
			color_printk(RED,GREEN,"[GT ERROR]");
			color_printk(RED,BLACK,"Render MMIO Sync Flush Status:%#010x\n",GT_IIR);	
		}

		if(GT_IIR & GT_RENDER_COMMAND_PARSER_MASTER_ERROR)				//3
		{
			color_printk(RED,GREEN,"[GT ERROR]");
			color_printk(RED,BLACK,"Render Command Parser Master Error:%#010x\n",GT_IIR);	
		}

		if(GT_IIR & GT_RENDER_PIPE_CONTROL_NOTIFY)					//4
		{
			color_printk(RED,GREEN,"[GT ERROR]");
			color_printk(RED,BLACK,"Render PIPE CONTROL Notify:%#010x\n",GT_IIR);	
		}

		if(GT_IIR & GT_RENDER_L3_PARITY_ERROR)						//5
		{
			color_printk(RED,GREEN,"[GT ERROR]");
			color_printk(RED,BLACK,"L3 Parity Error:%#010x\n",GT_IIR);	
		}

		if(GT_IIR & GT_RENDER_TIMEOUT_COUNTER_EXPIRED)					//6
		{
			color_printk(RED,GREEN,"[GT ERROR]");
			color_printk(RED,BLACK,"Render Timeout Counter Expired:%#010x\n",GT_IIR);	
		}

		if(GT_IIR & GT_RENDER_PAGE_DIRECTORY_FAULTS)					//7
		{
			color_printk(RED,GREEN,"[GT ERROR]");
			color_printk(RED,BLACK,"Render Page Directoy Faults:%#010x\n",GT_IIR);	
		}

		if(GT_IIR & GT_PREEMPTION_COMPLETE_INTERRUPT)					//8
		{
			color_printk(RED,GREEN,"[GT ERROR]");
			color_printk(RED,BLACK,"Preemption Complete Interrupt:%#010x\n",GT_IIR);	
		}

		if(GT_IIR & GT_RENDER_PREF_MONITOR_BUFFER_HALF_FULL_INTERRUPT)			//9
		{
			color_printk(RED,GREEN,"[GT ERROR]");
			color_printk(RED,BLACK,"Render Perf Monitor Buffer Half Full Interrupt:%#010x\n",GT_IIR);	
		}

		if(GT_IIR & GT_L3_COUNTER_SAVE)							//10
		{
			color_printk(RED,GREEN,"[GT ERROR]");
			color_printk(RED,BLACK,"L3 Counter Save:%#010x\n",GT_IIR);	
		}

		if(GT_IIR & GT_L3_PARITY_ERROR_SLICE1)						//11
		{
			color_printk(RED,GREEN,"[GT ERROR]");
			color_printk(RED,BLACK,"L3 Parity Error Slice1:%#010x\n",GT_IIR);	
		}

		if(GT_IIR & GT_VIDEO_CODEC_COMMAND_PARSER_USER_INTERRUPT)			//12
		{
			color_printk(RED,GREEN,"[GT ERROR]");
			color_printk(RED,BLACK,"VideoCodec Command Parser User Interrupt:%#010x\n",GT_IIR);	
		}

		if(GT_IIR & GT_VIDEO_CODEC_MMIO_SYNC_FLUSH_STATUS)				//14
		{
			color_printk(RED,GREEN,"[GT ERROR]");
			color_printk(RED,BLACK,"VideoCodec MMIO Sync Flush Status:%#010x\n",GT_IIR);	
		}

		if(GT_IIR & GT_VIDEO_CODEC_COMMAND_PARSER_MASTER_ERROR)				//15
		{
			color_printk(RED,GREEN,"[GT ERROR]");
			color_printk(RED,BLACK,"VideoCodec Command Parser Master Error:%#010x\n",GT_IIR);	
		}

		if(GT_IIR & GT_VIDEO_CODEC_MI_FLUSH_DW_NOTIFY)					//16
		{
			color_printk(RED,GREEN,"[GT ERROR]");
			color_printk(RED,BLACK,"VideoCodec MI FLUSH DW Notify:%#010x\n",GT_IIR);	
		}

		if(GT_IIR & GT_VIDEO_CODEC_TIMEOUT_COUNTER_EXPIRED)				//18
		{
			color_printk(RED,GREEN,"[GT ERROR]");
			color_printk(RED,BLACK,"VideoCodec Timeout Counter Expired:%#010x\n",GT_IIR);	
		}

		if(GT_IIR & GT_VIDEO_CODEC_PAGE_DIRECTORY_FAULTS)				//19
		{
			color_printk(RED,GREEN,"[GT ERROR]");
			color_printk(RED,BLACK,"VideoCodec Page Directory Faults:%#010x\n",GT_IIR);	
		}

		if(GT_IIR & GT_BLITTER_COMMAND_PARSER_USER_INTERRUPT)				//22
		{
			color_printk(RED,GREEN,"[GT ERROR]");
			color_printk(RED,BLACK,"Blitter Command Parser User Interrupt:%#010x\n",GT_IIR);	
		}

		if(GT_IIR & GT_BLITTER_MMIO_SYNC_FLUSH_STATUS)					//24
		{
			color_printk(RED,GREEN,"[GT ERROR]");
			color_printk(RED,BLACK,"Blitter MMIO Sync Flush Status:%#010x\n",GT_IIR);	
		}

		if(GT_IIR & GT_BLITTER_COMMAND_PARSER_MASTER_ERROR)				//25
		{
			color_printk(RED,GREEN,"[GT ERROR]");
			color_printk(RED,BLACK,"Blitter Command Parser Master Error:%#010x\n",GT_IIR);	
		}

		if(GT_IIR & GT_BLITTER_MI_FLUSH_DW_NOTIFY)					//26
		{
			color_printk(RED,GREEN,"[GT ERROR]");
			color_printk(RED,BLACK,"Blitter MI FLUSH DW Notify:%#010x\n",GT_IIR);	
		}

		if(GT_IIR & GT_BLITTER_PAGE_DIRECTORY_FAULTS)					//29
		{
			color_printk(RED,GREEN,"[GT ERROR]");
			color_printk(RED,BLACK,"Blitter Page Directory Faults:%#010x\n",GT_IIR);	
		}

		if(GT_IIR & (GT_BLITTER_COMMAND_PARSER_MASTER_ERROR | GT_VIDEO_CODEC_COMMAND_PARSER_MASTER_ERROR | GT_RENDER_COMMAND_PARSER_MASTER_ERROR))	//25 | 15 | 3
		{
			color_printk(RED,BLACK,"i915_capture_reg_state\n");
			color_printk(RED,BLACK,"GEN7_ERR_INT:%#010x\n",intel_uncore_read32(GTTMMADR,0x44040));
			color_printk(RED,BLACK,"FORCEWAKE_MT:%#010x\n",intel_uncore_read32(GTTMMADR,0xa188));
			color_printk(RED,BLACK,"DERRMR:%#010x\n",intel_uncore_read32(GTTMMADR,0x44050));
			color_printk(RED,BLACK,"ERROR_GEN6:%#010x\n",intel_uncore_read32(GTTMMADR,0x40a0));
			color_printk(RED,BLACK,"DONE_REG:%#010x\n",intel_uncore_read32(GTTMMADR,0x40b0));
			color_printk(RED,BLACK,"GAM_ECOCHK:%#010x\n",intel_uncore_read32(GTTMMADR,0x4090));
			color_printk(RED,BLACK,"GAC_ECO_BITS:%#010x\n",intel_uncore_read32(GTTMMADR,0x14090));
			color_printk(RED,BLACK,"CCID:%#010x\n",intel_uncore_read32(GTTMMADR,0x2180));
			color_printk(RED,BLACK,"DEIER:%#010x\n",intel_uncore_read32(GTTMMADR,0x4400c));
			color_printk(RED,BLACK,"GTIER:%#010x\n",intel_uncore_read32(GTTMMADR,0x4401c));
			color_printk(RED,BLACK,"EIR:%#010x\n",intel_uncore_read32(GTTMMADR,0x20b0));
			color_printk(RED,BLACK,"PGTBL_ER:%#010x\n",intel_uncore_read32(GTTMMADR,0x2024));

			color_printk(RED,BLACK,"i915_get_extra_instdone\n");
			color_printk(RED,BLACK,"GEN7_INSTDONE_1:%#010x\n",intel_uncore_read32(GTTMMADR,0x206c));
			color_printk(RED,BLACK,"GEN7_SC_INSTDONE:%#010x\n",intel_uncore_read32(GTTMMADR,0x7100));
			color_printk(RED,BLACK,"GEN7_SAMPLER_INSTDONE:%#010x\n",intel_uncore_read32(GTTMMADR,0xe160));
			color_printk(RED,BLACK,"GEN7_ROW_INSTDONE:%#010x\n",intel_uncore_read32(GTTMMADR,0xe164));

			color_printk(RED,BLACK,"i915_gem_record_fences\n");
			color_printk(RED,BLACK,"FENCE_REG_SANDYBRIDGE_0:%#010x\n",intel_uncore_read32(GTTMMADR,0x100000));

			color_printk(RED,BLACK,"i915_gem_record_rings\n");
			color_printk(RED,BLACK,"RC_PSMI_CONTROL:%#010x\n",intel_uncore_read32(GTTMMADR,0x2050));
			color_printk(RED,BLACK,"RING_FAULT_REG:%#010x\n",intel_uncore_read32(GTTMMADR,0x4094));
			color_printk(RED,BLACK,"RING_SYNC_0:%#010x\n",intel_uncore_read32(GTTMMADR,0x2040));
			color_printk(RED,BLACK,"RING_SYNC_1:%#010x\n",intel_uncore_read32(GTTMMADR,0x2044));
			color_printk(RED,BLACK,"RING_SYNC_2:%#010x\n",intel_uncore_read32(GTTMMADR,0x2048));
			color_printk(RED,BLACK,"RING_DMA_FADD:%#010x\n",intel_uncore_read32(GTTMMADR,0x2078));
			color_printk(RED,BLACK,"RING_IPEIR:%#010x\n",intel_uncore_read32(GTTMMADR,0x2064));
			color_printk(RED,BLACK,"RING_IPEHR:%#010x\n",intel_uncore_read32(GTTMMADR,0x2068));
			color_printk(RED,BLACK,"RING_INSTDONE:%#010x\n",intel_uncore_read32(GTTMMADR,0x206c));
			color_printk(RED,BLACK,"RING_INSTPS:%#010x\n",intel_uncore_read32(GTTMMADR,0x2070));
			color_printk(RED,BLACK,"RING_BBADDR:%#010x\n",intel_uncore_read32(GTTMMADR,0x2140));
			color_printk(RED,BLACK,"RING_BBSTATE:%#010x\n",intel_uncore_read32(GTTMMADR,0x2110));
			color_printk(RED,BLACK,"RING_INSTPM:%#010x\n",intel_uncore_read32(GTTMMADR,0x20c0));
			color_printk(RED,BLACK,"RING_ACTHD:%#010x\n",intel_uncore_read32(GTTMMADR,0x2074));
			color_printk(RED,BLACK,"READ_HEAD:%#010x\n",intel_uncore_read32(GTTMMADR,0x2034));
			color_printk(RED,BLACK,"READ_TAIL:%#010x\n",intel_uncore_read32(GTTMMADR,0x2030));
			color_printk(RED,BLACK,"READ_CTL:%#010x\n",intel_uncore_read32(GTTMMADR,0x203c));
			color_printk(RED,BLACK,"RENDER_HWS_PGA:%#010x\n",intel_uncore_read32(GTTMMADR,0x4080));
			color_printk(RED,BLACK,"RING_MODE_GEN7:%#010x\n",intel_uncore_read32(GTTMMADR,0x229c));
			color_printk(RED,BLACK,"RING_PP_DIR_BASE:%#010x\n",intel_uncore_read32(GTTMMADR,0x2228));

			color_printk(RED,BLACK,"i915_report_and_clear_eir\n");
			color_printk(RED,BLACK,"PGTBL_ER:%#010x\n",intel_uncore_read32(GTTMMADR,0x2024));
			color_printk(RED,BLACK,"INSTPM:%#010x\n",intel_uncore_read32(GTTMMADR,0x20c0));
			color_printk(RED,BLACK,"IPEIR:%#010x\n",intel_uncore_read32(GTTMMADR,0x2064));
			color_printk(RED,BLACK,"IPEHR:%#010x\n",intel_uncore_read32(GTTMMADR,0x2068));
			color_printk(RED,BLACK,"INSTPS:%#010x\n",intel_uncore_read32(GTTMMADR,0x2070));
			color_printk(RED,BLACK,"ACTHD:%#010x\n",intel_uncore_read32(GTTMMADR,0x2074));
			color_printk(RED,BLACK,"EIR:%#010x\n",intel_uncore_read32(GTTMMADR,0x20b0));
		}

		if(GT_IIR & (GT_RENDER_L3_PARITY_ERROR | GT_L3_PARITY_ERROR_SLICE1))	//5 | 11
		{
			color_printk(RED,BLACK,"ivybridge_parity_error_irq_handler\n");
			color_printk(RED,BLACK,"GTIMR:%#010x\n",intel_uncore_read32(GTTMMADR,0x44014));			
		}
	}

	DE_IIR = intel_uncore_read32(GTTMMADR,0x44008);			//DEIIR
	if(DE_IIR)
	{
		intel_uncore_write32(GTTMMADR,0x44008,DE_IIR);
		//Display Engine irq handler
		if(DE_IIR & DE_MASTER_INTERRUPT_CONTROL)					//31
		{
			color_printk(RED,GREEN,"[DE ERROR]");
			color_printk(RED,BLACK,"Master Interrupt Control:%#010x\n",DE_IIR);	
		}

		if(DE_IIR & DE_ERROR_INTERRUPT_COMBINED)					//30
		{
			unsigned int err_int;
			color_printk(RED,GREEN,"[DE ERROR]");
			color_printk(RED,BLACK,"Error Interrupts Combined:%#010x\n",DE_IIR);
			err_int = intel_uncore_read32(GTTMMADR,0x44040);
			color_printk(RED,BLACK,"GEN7_ERR_INT:%#010x\n",err_int);
			if(err_int & (1 << 31))
				color_printk(RED,BLACK,"Poison interrupt\n");
		}

		if(DE_IIR & DE_GSE)								//29
		{
			color_printk(RED,GREEN,"[DE ERROR]");
			color_printk(RED,BLACK,"GSE:%#010x\n",DE_IIR);	
		}

		if(DE_IIR & DE_PCH_DISPLAY_INTERRUPT_EVENT)					//28
		{
			color_printk(RED,GREEN,"[DE ERROR]");
			color_printk(RED,BLACK,"PCH Display interrupt event:%#010x\n",DE_IIR);
			color_printk(RED,BLACK,"SDEIIR:%#010x\n",intel_uncore_read32(GTTMMADR,0xc4008));
			color_printk(RED,BLACK,"SERR_INT:%#010x\n",intel_uncore_read32(GTTMMADR,0xc4040));
			color_printk(RED,BLACK,"PORT_HOTPLUG:%#010x\n",intel_uncore_read32(GTTMMADR,0xc4030));
		}

		if(DE_IIR & DE_DISPLAY_A_HOTPLUG)						//27
		{
			color_printk(RED,GREEN,"[DE ERROR]");
			color_printk(RED,BLACK,"DisplayPort A Hotplug:%#010x\n",DE_IIR);	
		}

		if(DE_IIR & DE_AUX_CHANNEL_A)							//26
		{
			color_printk(RED,GREEN,"[DE ERROR]");
			color_printk(RED,BLACK,"AUX Channel A:%#010x\n",DE_IIR);	
		}

		if(DE_IIR & DE_DPST_HISTOGRAM_EVENT)						//25
		{
			color_printk(RED,GREEN,"[DE ERROR]");
			color_printk(RED,BLACK,"DPST histogram event:%#010x\n",DE_IIR);	
		}

		if(DE_IIR & DE_DPST_PHASE_IN_EVENT)						//24
		{
			color_printk(RED,GREEN,"[DE ERROR]");
			color_printk(RED,BLACK,"DPST phase in event:%#010x\n",DE_IIR);	
		}

		if(DE_IIR & DE_AUDIO_CODEC_INTERRUPTS_COMBINED)					//20
		{
			color_printk(RED,GREEN,"[DE ERROR]");
			color_printk(RED,BLACK,"Audio Codec Interrupts Combined:%#010x\n",DE_IIR);	
		}

		if(DE_IIR & DE_SRD_INTERRUPTS_COMBINED)						//19
		{
			color_printk(RED,GREEN,"[DE ERROR]");
			color_printk(RED,BLACK,"SRD Interrupts Combined:%#010x\n",DE_IIR);	
		}

		if(DE_IIR & DE_GTC_CPU_INTERRUPTS_COMBINED)					//15
		{
			color_printk(RED,GREEN,"[DE ERROR]");
			color_printk(RED,BLACK,"GTC CPU Interrupts Combined:%#010x\n",DE_IIR);	
		}

		if(DE_IIR & DE_SPRITE_PLANE_FLIP_DONE_C)					//14
		{
			color_printk(RED,GREEN,"[DE ERROR]");
			color_printk(RED,BLACK,"Sprite Plane Flip Done C:%#010x\n",DE_IIR);	
		}

		if(DE_IIR & DE_PRIMARY_PLANE_FLIP_DONE_C)					//13
		{
			color_printk(RED,GREEN,"[DE ERROR]");
			color_printk(RED,BLACK,"Primary Plane Flip Done C:%#010x\n",DE_IIR);	
		}

		if(DE_IIR & DE_LINE_COMPARE_PIPE_C)						//12
		{
			color_printk(RED,GREEN,"[DE ERROR]");
			color_printk(RED,BLACK,"Line Compare Pipe C:%#010x\n",DE_IIR);	
		}

		if(DE_IIR & DE_V_SYNC_PIPE_C)							//11
		{
			color_printk(RED,GREEN,"[DE ERROR]");
			color_printk(RED,BLACK,"Vsync Pipe C:%#010x\n",DE_IIR);	
		}

		if(DE_IIR & DE_V_BLANK_PIPE_C)							//10
		{
			color_printk(RED,GREEN,"[DE ERROR]");
			color_printk(RED,BLACK,"Vblank Pipe C:%#010x\n",DE_IIR);	
		}

		if(DE_IIR & DE_SPRITE_PLANE_FLIP_DONE_B)					//9
		{
			color_printk(RED,GREEN,"[DE ERROR]");
			color_printk(RED,BLACK,"Sprite Plane Flip Done B:%#010x\n",DE_IIR);	
		}

		if(DE_IIR & DE_PRIMARY_PLANE_FLIP_DONE_B)					//8
		{
			color_printk(RED,GREEN,"[DE ERROR]");
			color_printk(RED,BLACK,"Primary Plane Flip Done B:%#010x\n",DE_IIR);	
		}

		if(DE_IIR & DE_LINE_COMPARE_PIPE_B)						//7
		{
			color_printk(RED,GREEN,"[DE ERROR]");
			color_printk(RED,BLACK,"Line Compare Pipe B:%#010x\n",DE_IIR);	
		}

		if(DE_IIR & DE_V_SYNC_PIPE_B)							//6
		{
			color_printk(RED,GREEN,"[DE ERROR]");
			color_printk(RED,BLACK,"Vsync Pipe B:%#010x\n",DE_IIR);	
		}

		if(DE_IIR & DE_V_BLANK_PIPE_B)							//5
		{
			color_printk(RED,GREEN,"[DE ERROR]");
			color_printk(RED,BLACK,"Vblank Pipe B:%#010x\n",DE_IIR);	
		}

		if(DE_IIR & DE_SPRITE_PLANE_FLIP_DONE_A)					//4
		{
			color_printk(RED,GREEN,"[DE ERROR]");
			color_printk(RED,BLACK,"Sprite Plane Flip Done A:%#010x\n",DE_IIR);	
		}

		if(DE_IIR & DE_PRIMARY_PLANE_FLIP_DONE_A)					//3
		{
			color_printk(RED,GREEN,"[DE ERROR]");
			color_printk(RED,BLACK,"Primary Plane Flip Done A:%#010x\n",DE_IIR);	
		}

		if(DE_IIR & DE_LINE_COMPARE_PIPE_A)						//2
		{
			color_printk(RED,GREEN,"[DE ERROR]");
			color_printk(RED,BLACK,"Line Compare Pipe A:%#010x\n",DE_IIR);	
		}

		if(DE_IIR & DE_V_SYNC_PIPE_A)							//1
		{
			color_printk(RED,GREEN,"[DE ERROR]");
			color_printk(RED,BLACK,"Vsync Pipe A:%#010x\n",DE_IIR);	
		}

		if(DE_IIR & DE_V_BLANK_PIPE_A)							//0
		{
			color_printk(RED,GREEN,"[DE ERROR]");
			color_printk(RED,BLACK,"Vblank Pipe A:%#010x\n",DE_IIR);	
		}
	}

	PM_IIR = intel_uncore_read32(GTTMMADR,0x44028);			//PMIIR
	if(PM_IIR)
	{
		intel_uncore_write32(GTTMMADR,0x44028,PM_IIR);
		//Power Management irq handler
		if(PM_IIR & PM_RENDER_GEYSERVILLE_DOWN_EVALUATION_INTERVAL)			//1
		{
			color_printk(RED,GREEN,"[PM ERROR]");
			color_printk(RED,BLACK,"Render Geyserville Down Evaluation Interval:%#010x\n",PM_IIR);	
		}

		if(PM_IIR & PM_RENDER_GEYSERVILLE_UP_EVALUATION_INTERVAL)			//2
		{
			color_printk(RED,GREEN,"[PM ERROR]");
			color_printk(RED,BLACK,"Render Geyserville UP Evaluation Interval:%#010x\n",PM_IIR);	
		}

		if(PM_IIR & PM_RP_DOWN_THRESHOLD)						//4
		{
			color_printk(RED,GREEN,"[PM ERROR]");
			color_printk(RED,BLACK,"RP DOWN Threshold:%#010x\n",PM_IIR);	
		}

		if(PM_IIR & PM_RP_UP_THRESHOLD)							//5
		{
			color_printk(RED,GREEN,"[PM ERROR]");
			color_printk(RED,BLACK,"RP UP Threshold:%#010x\n",PM_IIR);	
		}

		if(PM_IIR & PM_RENDER_FREQUENCY_DOWNWARD_TIMEOUT_DURING_RC6)			//6
		{
			color_printk(RED,GREEN,"[PM ERROR]");
			color_printk(RED,BLACK,"Render Frequency Downward Timeout During RC6:%#010x\n",PM_IIR);	
		}

		if(PM_IIR & PM_VIDEOENH_COMMAND_PARSER_USER_INTERRUPT)				//10
		{
			color_printk(RED,GREEN,"[PM ERROR]");
			color_printk(RED,BLACK,"VideoEnh Command Parser User Interrupt:%#010x\n",PM_IIR);	
		}

		if(PM_IIR & PM_VIDEOENH_MMIO_SYNC_FLUSH_STATUS)					//11
		{
			color_printk(RED,GREEN,"[PM ERROR]");
			color_printk(RED,BLACK,"VideoEnh MMIO Sync Flush Status:%#010x\n",PM_IIR);	
		}

		if(PM_IIR & PM_VIDEOENH_COMMAND_PARSER_MASTER_ERROR)				//12
		{
			color_printk(RED,GREEN,"[PM ERROR]");
			color_printk(RED,BLACK,"VideoEnh Command Parser Master Error:%#010x\n",PM_IIR);	
		}

		if(PM_IIR & PM_VIDEOENH_MI_FLUSH_DW_NOTIFY)					//13
		{
			color_printk(RED,GREEN,"[PM ERROR]");
			color_printk(RED,BLACK,"VideoEnh MI FLUSH DW Notify:%#010x\n",PM_IIR);	
		}

		if(PM_IIR & PM_PCU_THERMAL_EVENT)						//24
		{
			color_printk(RED,GREEN,"[PM ERROR]");
			color_printk(RED,BLACK,"PCU Thermal Event:%#010x\n",PM_IIR);	
		}

		if(PM_IIR & PM_PCU_PCODE2DRIVER_MAILBOX_EVENT)					//25
		{
			color_printk(RED,GREEN,"[PM ERROR]");
			color_printk(RED,BLACK,"PCU Pcode2driver Mailbox Event:%#010x\n",PM_IIR);	
		}
	}

	intel_uncore_write32(GTTMMADR,0x4400c,DE_IER);			//DEIER
	intel_uncore_read32(GTTMMADR,0x4400c);

	intel_uncore_write32(GTTMMADR,0xc400c,SDE_IER);			//SDEIER
	intel_uncore_read32(GTTMMADR,0xc400c);
}
