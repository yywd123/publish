/***************************************************
*		 Copyright (c) 2018 MINE 田宇
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of version 2 of the GNU General Public
* License as published by the Free Software Foundation.
*
***************************************************/

#ifndef __GPU_H__

#define __GPU_H__

/*

*/
//	GGTT
#define GGTT_CACHEABILITY_CONTROL(bits)	((((bits) & 0x7) << 1) | (((bits) & 0x8) << (11 - 3)))

#define GGTT_PTE_VALID				(1)
#define RING_VALID				(1)

#define GGTT_PTE_UNCACHED			(0)

#define GGTT_WB_ELLC_AGE3			GGTT_CACHEABILITY_CONTROL(0xc)
#define GGTT_WB_ELLC_AGE2			GGTT_CACHEABILITY_CONTROL(0xd)
#define GGTT_WB_ELLC_AGE1			GGTT_CACHEABILITY_CONTROL(0xe)
#define GGTT_WB_ELLC_AGE0			GGTT_CACHEABILITY_CONTROL(0xf)

#define GGTT_WB_ELLC_LLC_AGE3			GGTT_CACHEABILITY_CONTROL(0x8)
#define GGTT_WB_ELLC_LLC_AGE2			GGTT_CACHEABILITY_CONTROL(0x9)
#define GGTT_WB_ELLC_LLC_AGE1			GGTT_CACHEABILITY_CONTROL(0xa)
#define GGTT_WB_ELLC_LLC_AGE0			GGTT_CACHEABILITY_CONTROL(0xb)

#define GGTT_WT_ELLC_LLC_AGE3			GGTT_CACHEABILITY_CONTROL(0x7)
#define GGTT_WT_ELLC_LLC_AGE0			GGTT_CACHEABILITY_CONTROL(0x6)
#define GGTT_WT_ELLC_AGE3			GGTT_CACHEABILITY_CONTROL(0x5)
#define GGTT_WT_ELLC_AGE0			GGTT_CACHEABILITY_CONTROL(0x4)

#define GGTT_WB_LLC_AGE3			GGTT_CACHEABILITY_CONTROL(0x2)
#define GGTT_WB_LLC_AGE0			GGTT_CACHEABILITY_CONTROL(0x3)

#define GGTT_PTE_ADDR_ENCODE(addr)		((addr) | (((addr) >> 28) & 0x7f0))

//	GT Interrupt Bit Definition
//	alse see Bit Definition for Interrupt Control Registers - Render
#define GT_BLITTER_PAGE_DIRECTORY_FAULTS			(1 << 29)
#define GT_BLITTER_MI_FLUSH_DW_NOTIFY				(1 << 26)
#define GT_BLITTER_COMMAND_PARSER_MASTER_ERROR			(1 << 25)
#define GT_BLITTER_MMIO_SYNC_FLUSH_STATUS			(1 << 24)
#define GT_BLITTER_COMMAND_PARSER_USER_INTERRUPT		(1 << 22)

#define GT_VIDEO_CODEC_PAGE_DIRECTORY_FAULTS			(1 << 19)
#define GT_VIDEO_CODEC_TIMEOUT_COUNTER_EXPIRED			(1 << 18)
#define GT_VIDEO_CODEC_MI_FLUSH_DW_NOTIFY			(1 << 16)
#define GT_VIDEO_CODEC_COMMAND_PARSER_MASTER_ERROR		(1 << 15)
#define GT_VIDEO_CODEC_MMIO_SYNC_FLUSH_STATUS			(1 << 14)
#define GT_VIDEO_CODEC_COMMAND_PARSER_USER_INTERRUPT		(1 << 12)

#define GT_L3_PARITY_ERROR_SLICE1				(1 << 11)
#define GT_L3_COUNTER_SAVE					(1 << 10)
#define GT_RENDER_PREF_MONITOR_BUFFER_HALF_FULL_INTERRUPT	(1 << 9)
#define GT_PREEMPTION_COMPLETE_INTERRUPT			(1 << 8)
#define GT_RENDER_PAGE_DIRECTORY_FAULTS				(1 << 7)
#define GT_RENDER_TIMEOUT_COUNTER_EXPIRED			(1 << 6)
#define GT_RENDER_L3_PARITY_ERROR				(1 << 5)
#define GT_RENDER_PIPE_CONTROL_NOTIFY				(1 << 4)
#define GT_RENDER_COMMAND_PARSER_MASTER_ERROR			(1 << 3)
#define GT_RENDER_MMIO_SYNC_FLUSH_STATUS			(1 << 2)
#define GT_RENDER_DEBUG_INTERRUPT				(1 << 1)
#define GT_RENDER_COMMAND_PARSER_USER_INTERRUPT			(1 << 0)

//	Display Engine Interrupt Bit Definition
#define DE_MASTER_INTERRUPT_CONTROL				(1 << 31)

#define DE_ERROR_INTERRUPT_COMBINED				(1 << 30)
#define DE_GSE							(1 << 29)
#define DE_PCH_DISPLAY_INTERRUPT_EVENT				(1 << 28)
#define DE_DISPLAY_A_HOTPLUG					(1 << 27)
#define DE_AUX_CHANNEL_A					(1 << 26)
#define DE_DPST_HISTOGRAM_EVENT					(1 << 25)
#define DE_DPST_PHASE_IN_EVENT					(1 << 24)
#define DE_AUDIO_CODEC_INTERRUPTS_COMBINED			(1 << 20)
#define DE_SRD_INTERRUPTS_COMBINED				(1 << 19)
#define DE_GTC_CPU_INTERRUPTS_COMBINED				(1 << 15)

#define DE_SPRITE_PLANE_FLIP_DONE_C				(1 << 14)
#define DE_PRIMARY_PLANE_FLIP_DONE_C				(1 << 13)
#define DE_LINE_COMPARE_PIPE_C					(1 << 12)
#define DE_V_SYNC_PIPE_C					(1 << 11)
#define DE_V_BLANK_PIPE_C					(1 << 10)

#define DE_SPRITE_PLANE_FLIP_DONE_B				(1 << 9)
#define DE_PRIMARY_PLANE_FLIP_DONE_B				(1 << 8)
#define DE_LINE_COMPARE_PIPE_B					(1 << 7)
#define DE_V_SYNC_PIPE_B					(1 << 6)
#define DE_V_BLANK_PIPE_B					(1 << 5)

#define DE_SPRITE_PLANE_FLIP_DONE_A				(1 << 4)
#define DE_PRIMARY_PLANE_FLIP_DONE_A				(1 << 3)
#define DE_LINE_COMPARE_PIPE_A					(1 << 2)
#define DE_V_SYNC_PIPE_A					(1 << 1)
#define DE_V_BLANK_PIPE_A					(1 << 0)

//	Power Management Interrupt Bit Definition
#define PM_PCU_PCODE2DRIVER_MAILBOX_EVENT			(1 << 25)
#define PM_PCU_THERMAL_EVENT					(1 << 24)

#define PM_VIDEOENH_MI_FLUSH_DW_NOTIFY				(1 << 13)
#define PM_VIDEOENH_COMMAND_PARSER_MASTER_ERROR			(1 << 12)
#define PM_VIDEOENH_MMIO_SYNC_FLUSH_STATUS			(1 << 11)
#define PM_VIDEOENH_COMMAND_PARSER_USER_INTERRUPT		(1 << 10)

#define PM_RENDER_FREQUENCY_DOWNWARD_TIMEOUT_DURING_RC6		(1 << 6)
#define PM_RP_UP_THRESHOLD					(1 << 5)
#define PM_RP_DOWN_THRESHOLD					(1 << 4)

#define PM_RENDER_GEYSERVILLE_UP_EVALUATION_INTERVAL		(1 << 2)
#define PM_RENDER_GEYSERVILLE_DOWN_EVALUATION_INTERVAL		(1 << 1)


typedef struct {unsigned int ggtt_pt;} gtt_pt_t;
#define	mk_ggtt_pt(addr,attr)	((unsigned int)(addr) | (unsigned int)(attr))
#define set_ggtt_pt(ggtt_pt_ptr,ggtt_pt_val)	(*(ggtt_pt_ptr) = (ggtt_pt_val))


typedef struct {unsigned int ppgtt_pdt;} ppgtt_pdt_t;
#define	mk_ppgtt_pdt(addr,attr)	((unsigned int)(addr) | (unsigned int)(attr))
#define set_ppgtt_pdt(ppgtt_pdt_ptr,ppgtt_pdt_val)	(*(ppgtt_pdt_ptr) = (ppgtt_pdt_val))

typedef struct {unsigned int ppgtt_pt;} ppgtt_pt_t;
#define	mk_ppgtt_pt(addr,attr)	((unsigned int)(addr) | (unsigned int)(attr))
#define set_ppgtt_pt(ppgtt_pt_ptr,ppgtt_pt_val)	(*(ppgtt_pt_ptr) = (ppgtt_pt_val))

inline unsigned long intel_uncore_read64(unsigned char *uncore, unsigned int reg)
{
	return *(unsigned long *)(uncore + reg);
}

inline void intel_uncore_write64(unsigned char *uncore, unsigned int reg, unsigned long val)
{
	*(unsigned long *)(uncore + reg) = val;
}

inline unsigned int intel_uncore_read32(unsigned char *uncore, unsigned int reg)
{
	return *(unsigned int *)(uncore + reg);
}

inline void intel_uncore_write32(unsigned char *uncore, unsigned int reg, unsigned int val)
{
	*(unsigned int *)(uncore + reg) = val;
}

void init_GPU(void);

#endif
