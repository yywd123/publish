/***************************************************
*		 Copyright (c) 2018 MINE 田宇
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of version 2 of the GNU General Public
* License as published by the Free Software Foundation.
*
***************************************************/

#ifndef __GPUID_H__

#define __GPUID_H__

enum gpu_platform
{
	GEN_UNKNOWN = 0,
	GEN1,
	GEN2,
	GEN3,
	GEN4,
	GEN5,
	GEN6,
	GEN7,
	GEN8,
	GEN9,
	GEN10,
	GEN11,
	GEN12,
};

struct pci_device_id
{
	unsigned short	VendorID;
	unsigned short	DeviceID;
	enum gpu_platform platform;
	char * name;
};

struct pci_device_id GPU_id_list[]=
{
//	I810
	{0x8086,0x7121,GEN_UNKNOWN,"I810"},
	{0x8086,0x7123,GEN_UNKNOWN,"I810_DC100"},
	{0x8086,0x7125,GEN_UNKNOWN,"I810E"},

//	I815
	{0x8086,0x1132,GEN_UNKNOWN,"I815"},

//	I830
	{0x8086,0x3577,GEN2,"i830"},

//	I845G
	{0x8086,0x2562,GEN2,"i845G"},

//	I85X
	{0x8086,0x3582,GEN2,"I855GM"},
	{0x8086,0x358e,GEN2,"i855G"},

//	I865G
	{0x8086,0x2572,GEN2,"i865G"},

//	I915G
	{0x8086,0x2582,GEN3,"i915G"},
	{0x8086,0x258a,GEN2,"E7221G"},

//	I915GM
	{0x8086,0x2592,GEN3,"i915GM"},

//	I945G
	{0x8086,0x2772,GEN3,"i945G"},

//	I945GM
	{0x8086,0x27a2,GEN3,"i945GM"},
	{0x8086,0x27ae,GEN3,"i945GME"},

//	I965G
	{0x8086,0x2972,GEN3,"i946GZ"},
	{0x8086,0x2982,GEN3,"G35G"},
	{0x8086,0x2992,GEN4,"i965Q"},
	{0x8086,0x29a2,GEN4,"i965G"},

//	G33
	{0x8086,0x29b2,GEN3,"G33G"},
	{0x8086,0x29c2,GEN3,"Q35G"},
	{0x8086,0x29d2,GEN3,"Q33G"},

//	I965GM
	{0x8086,0x2a02,GEN4,"i965GM"},
	{0x8086,0x2a12,GEN4,"i965GME"},

//	GM45
	{0x8086,0x2a42,GEN4,"GM45"},

//	G45
	{0x8086,0x2e02,GEN4,"IGD_EG"},
	{0x8086,0x2e12,GEN4,"Q45G"},
	{0x8086,0x2e22,GEN4,"G45G"},
	{0x8086,0x2e32,GEN4,"G41G"},
	{0x8086,0x2e42,GEN4,"B43G"},
	{0x8086,0x2e92,GEN4,"B43G.1"},

//	PineView G
	{0x8086,0xa001,GEN3,"Atom D4xx"},

//	PineView Mobile
	{0x8086,0xa011,GEN3,"Atom N4xx"},

//	IronLake Desktop
	{0x8086,0x0042,GEN5,"IronLake Desktop"},

//	IronLake Mobile
	{0x8086,0x0046,GEN5,"IronLake Mobile"},

//	SandyBridge Desktop GT1
	{0x8086,0x0102,GEN6,"SandyBridge Desktop GT1"},
	{0x8086,0x010a,GEN6,"SandyBridge Server GT1"},

//	SandyBridge Desktop GT2
	{0x8086,0x0112,GEN6,"SandyBridge Desktop GT2"},
	{0x8086,0x0122,GEN6,"SandyBridge Desktop GT2+"},

//	SandyBridge Mobile GT1
	{0x8086,0x0106,GEN6,"SandyBridge Mobile GT1"},

//	SandyBridge Mobile GT2
	{0x8086,0x0116,GEN6,"SandyBridge Mobile GT2"},
	{0x8086,0x0126,GEN6,"SandyBridge Mobile GT2+"},

//	IvyBridge Mobile  GT1
	{0x8086,0x0156,GEN7,"IvyBridge Mobile GT1"},

//	IvyBridge Mobile  GT2
	{0x8086,0x0166,GEN7,"IvyBridge Mobile GT2"},

//	IvyBridge Desktop  GT1
	{0x8086,0x0152,GEN7,"IvyBridge Desktop GT1"},
	{0x8086,0x015a,GEN7,"IvyBridge Server GT1"},

//	IvyBridge Desktop  GT2
	{0x8086,0x0162,GEN7,"IvyBridge Desktop GT2"},
	{0x8086,0x016a,GEN7,"IvyBridge Server GT2"},

//	Haswell ULT GT1
	{0x8086,0x0A02,GEN7,"Haswell Desktop ULT GT1"},
	{0x8086,0x0A0A,GEN7,"Haswell Server ULT GT1"},
	{0x8086,0x0A0B,GEN7,"Haswell Reserved ULT GT1"},
	{0x8086,0x0A06,GEN7,"Haswell Mobile ULT GT1"},

//	Haswell ULX GT1
	{0x8086,0x0A0E,GEN7,"Haswell Mobile ULX GT1"},

//	Haswell GT1
	{0x8086,0x0402,GEN7,"Haswell Desktop GT1"},
	{0x8086,0x040a,GEN7,"Haswell Server GT1"},
	{0x8086,0x040B,GEN7,"Haswell Reserved GT1"},
	{0x8086,0x040E,GEN7,"Haswell Reserved GT1"},
	{0x8086,0x0C02,GEN7,"Haswell Desktop SDV GT1"},
	{0x8086,0x0C0A,GEN7,"Haswell Server SDV GT1"},
	{0x8086,0x0C0B,GEN7,"Haswell Reserved SDV GT1"},
	{0x8086,0x0C0E,GEN7,"Haswell Reserved SDV GT1"},
	{0x8086,0x0D02,GEN7,"Haswell Desktop CRW GT1"},
	{0x8086,0x0D0A,GEN7,"Haswell Server CRW GT1"},
	{0x8086,0x0D0B,GEN7,"Haswell Reserved CRW GT1"},
	{0x8086,0x0D0E,GEN7,"Haswell Reserved CRW GT1"},
	{0x8086,0x0406,GEN7,"Haswell Mobile GT1"},
	{0x8086,0x0C06,GEN7,"Haswell Mobile SDV GT1"},
	{0x8086,0x0D06,GEN7,"Haswell Mobile CRW GT1"},

//	Haswell ULT GT2
	{0x8086,0x0A12,GEN7,"Haswell Desktop ULT GT2"},
	{0x8086,0x0A1A,GEN7,"Haswell Server ULT GT2"},
	{0x8086,0x0A1B,GEN7,"Haswell Reserved ULT GT2"},
	{0x8086,0x0A16,GEN7,"Haswell Mobile ULT GT2"},

//	Haswell ULX GT2
	{0x8086,0x0A1E,GEN7,"Haswell Mobile ULX GT2"},

//	Haswell GT2
	{0x8086,0x0412,GEN7,"Haswell Desktop GT2"},
	{0x8086,0x041a,GEN7,"Haswell Server GT2"},
	{0x8086,0x041b,GEN7,"Haswell Reserved GT2"},
	{0x8086,0x041e,GEN7,"Haswell Reserved GT2"},
	{0x8086,0x0C12,GEN7,"Haswell Desktop SDV GT2"},
	{0x8086,0x0C1A,GEN7,"Haswell Server SDV GT2"},
	{0x8086,0x0C1B,GEN7,"Haswell Reserved SDV GT2"},
	{0x8086,0x0C1E,GEN7,"Haswell Reserved SDV GT2"},
	{0x8086,0x0D12,GEN7,"Haswell Desktop CRW GT2"},
	{0x8086,0x0D1A,GEN7,"Haswell Server CRW GT2"},
	{0x8086,0x0D1B,GEN7,"Haswell Reserved CRW GT2"},
	{0x8086,0x0D1E,GEN7,"Haswell Reserved CRW GT2"},
	{0x8086,0x0416,GEN7,"Haswell Mobile GT2"},
	{0x8086,0x0426,GEN7,"Haswell Mobile GT2"},
	{0x8086,0x0C16,GEN7,"Haswell Mobile SDV GT2"},
	{0x8086,0x0D16,GEN7,"Haswell Mobile CRW GT2"},

//	Haswell ULT GT3
	{0x8086,0x0A22,GEN7,"Haswell Desktop ULT GT3"},
	{0x8086,0x0A2A,GEN7,"Haswell Server ULT GT3"},
	{0x8086,0x0A2B,GEN7,"Haswell Reserved ULT GT3"},
	{0x8086,0x0A26,GEN7,"Haswell Mobile ULT GT3"},
	{0x8086,0x0A2E,GEN7,"Haswell Reserved ULT GT3"},

//	Haswell GT3
	{0x8086,0x0422,GEN7,"Haswell desktop GT3"},
	{0x8086,0x042a,GEN7,"Haswell server GT3"},
	{0x8086,0x042B,GEN7,"Haswell reserved GT3"},
	{0x8086,0x042E,GEN7,"Haswell reserved GT3"},
	{0x8086,0x0C22,GEN7,"Haswell desktop SDV GT3"},
	{0x8086,0x0C2A,GEN7,"Haswell server SDV GT3"},
	{0x8086,0x0C2B,GEN7,"Haswell reserved SDV GT3"},
	{0x8086,0x0C2E,GEN7,"Haswell reserved SDV GT3"},
	{0x8086,0x0D22,GEN7,"Haswell desktop SDV GT3"},
	{0x8086,0x0D2A,GEN7,"Haswell server SDV GT3"},
	{0x8086,0x0D2B,GEN7,"Haswell reserved SDV GT3"},
	{0x8086,0x0D2E,GEN7,"Haswell reserved SDV GT3"},
	{0x8086,0x0C26,GEN7,"Haswell mobile SDV GT3"},
	{0x8086,0x0D26,GEN7,"Haswell mobile SDV GT3"},

//	ValleyView
	{0x8086,0x0f30,GEN7,"ValleyView"},
	{0x8086,0x0f31,GEN7,"ValleyView"},
	{0x8086,0x0f32,GEN7,"ValleyView"},
	{0x8086,0x0f33,GEN7,"ValleyView"},
	{0x8086,0x0157,GEN7,"ValleyView"},
	{0x8086,0x0155,GEN7,"ValleyView"},

//	Broadwell ULT GT1
	{0x8086,0x1606,GEN8,"Broadwell ULT GT1"},
	{0x8086,0x160B,GEN8,"Broadwell Iris GT1"},

//	Broadwell ULX GT1
	{0x8086,0x160E,GEN8,"Broadwell ULX GT1"},

//	Broadwell GT1
	{0x8086,0x1602,GEN8,"Broadwell ULT GT1"},
	{0x8086,0x160A,GEN8,"Broadwell Server GT1"},
	{0x8086,0x160D,GEN8,"Broadwell Workstation GT1"},

//	Broadwell ULT GT2
	{0x8086,0x1616,GEN8,"Broadwell ULT GT2"},
	{0x8086,0x161B,GEN8,"Broadwell ULT GT2"},

//	Broadwell ULX GT2
	{0x8086,0x161E,GEN8,"Broadwell ULX GT2"},

//	Broadwell GT2
	{0x8086,0x1612,GEN8,"Broadwell Halo GT2"},
	{0x8086,0x161A,GEN8,"Broadwell Server GT2"},
	{0x8086,0x161D,GEN8,"Broadwell Workstation GT2"},

//	Broadwell ULT GT3
	{0x8086,0x1626,GEN8,"Broadwell ULT GT3"},
	{0x8086,0x162B,GEN8,"Broadwell Iris GT3"},

//	Broadwell ULX GT3
	{0x8086,0x162E,GEN8,"Broadwell ULX GT3"},

//	Broadwell GT3
	{0x8086,0x1622,GEN8,"Broadwell ULT GT3"},
	{0x8086,0x162A,GEN8,"Broadwell Server GT3"},
	{0x8086,0x162D,GEN8,"Broadwell Workstation GT3"},

//	Broadwell ULT RESERVED
	{0x8086,0x1636,GEN8,"Broadwell ULT RSVD"},
	{0x8086,0x163B,GEN8,"Broadwell Iris RSVD"},

//	Broadwell ULX RESERVED
	{0x8086,0x163E,GEN8,"Broadwell ULX RSVD"},

//	Broadwell RESERVED
	{0x8086,0x1632,GEN8,"Broadwell ULT RSVD"},
	{0x8086,0x163A,GEN8,"Broadwell Server RSVD"},
	{0x8086,0x163D,GEN8,"Broadwell Workstation RSVD"},

//	CherryView
	{0x8086,0x22b0,GEN8,"CherryView"},
	{0x8086,0x22b1,GEN8,"CherryView"},
	{0x8086,0x22b2,GEN8,"CherryView"},
	{0x8086,0x22b3,GEN8,"CherryView"},

//	Skylake ULT GT1
	{0x8086,0x1906,GEN9,"Skylake ULT GT1"},

//	Skylake ULX GT1
	{0x8086,0x190E,GEN9,"Skylake ULX GT1"},

//	Skylake GT1
	{0x8086,0x1902,GEN9,"Skylake Desktop GT1"},
	{0x8086,0x190B,GEN9,"Skylake Halo GT1"},
	{0x8086,0x190A,GEN9,"Skylake Server GT1"},

//	Skylake ULT GT2
	{0x8086,0x1916,GEN9,"Skylake ULT GT2"},
	{0x8086,0x1921,GEN9,"Skylake ULT GT2F"},

//	Skylake ULX GT2
	{0x8086,0x191E,GEN9,"Skylake ULX GT2"},

//	Skylake GT2
	{0x8086,0x1912,GEN9,"Skylake Desktop GT2"},
	{0x8086,0x191B,GEN9,"Skylake Halo GT2"},
	{0x8086,0x191A,GEN9,"Skylake Server GT2"},
	{0x8086,0x191D,GEN9,"Skylake Workstation GT2"},

//	Skylake ULT GT3
	{0x8086,0x1926,GEN9,"Skylake ULT GT3"},
	{0x8086,0x1923,GEN9,"Skylake ULT GT3"},
	{0x8086,0x1927,GEN9,"Skylake ULT GT3"},

//	Skylake GT3
	{0x8086,0x192B,GEN9,"Skylake Halo GT3"},
	{0x8086,0x192D,GEN9,"Skylake Server GT3"},

//	Skylake GT4
	{0x8086,0x1932,GEN9,"Skylake Desktop GT4"},
	{0x8086,0x193B,GEN9,"Skylake Halo GT4"},
	{0x8086,0x193D,GEN9,"Skylake Workstation GT4"},
	{0x8086,0x192A,GEN9,"Skylake Server GT4"},
	{0x8086,0x193A,GEN9,"Skylake Server GT4e"},

//	Broxton
	{0x8086,0x0A84,GEN9,"Broxton"},
	{0x8086,0x1A84,GEN9,"Broxton"},
	{0x8086,0x1A85,GEN9,"Broxton"},
	{0x8086,0x5A84,GEN9,"Broxton"},
	{0x8086,0x5A85,GEN9,"Broxton"},

//	GoldenLake
	{0x8086,0x3184,GEN9,"GoldenLake"},
	{0x8086,0x3185,GEN9,"GoldenLake"},

//	KabyLake ULT GT1
	{0x8086,0x5906,GEN9,"KabyLake ULT GT1"},
	{0x8086,0x5913,GEN9,"KabyLake ULT GT1.5"},

//	KabyLake ULX GT1
	{0x8086,0x590E,GEN9,"KabyLake ULX GT1"},
	{0x8086,0x5915,GEN9,"KabyLake ULX GT1.5"},

//	KabyLake
	{0x8086,0x5902,GEN9,"KabyLake DT  GT1"},
	{0x8086,0x5908,GEN9,"KabyLake Halo GT1"},
	{0x8086,0x590B,GEN9,"KabyLake Halo GT1"},
	{0x8086,0x590A,GEN9,"KabyLake SRV GT1"},

//	KabyLake ULT GT2
	{0x8086,0x5916,GEN9,"KabyLake ULT GT2"},
	{0x8086,0x5921,GEN9,"KabyLake ULT GT2F"},

//	KabyLake ULX GT2
	{0x8086,0x591E,GEN9,"KabyLake ULX GT2"},

//	KabyLake GT2
	{0x8086,0x5917,GEN9,"KabyLake Mobile GT2"},
	{0x8086,0x5912,GEN9,"KabyLake DT GT2"},
	{0x8086,0x591B,GEN9,"KabyLake Halo GT2"},
	{0x8086,0x591A,GEN9,"KabyLake SRV GT2"},
	{0x8086,0x591D,GEN9,"KabyLake WKS GT2"},

//	KabyLake ULT GT3
	{0x8086,0x5926,GEN9,"KabyLake ULT GT3"},
	{0x8086,0x5923,GEN9,"KabyLake ULT GT3"},
	{0x8086,0x5927,GEN9,"KabyLake ULT GT3"},

//	KabyLake GT4
	{0x8086,0x593B,GEN9,"KabyLake Halo GT4"},

//	AML/KBL Y GT2
	{0x8086,0x591C,GEN9,"KabyLake ULX GT2"},
	{0x8086,0x87C0,GEN9,"KabyLake ULX GT2"},

//	AML/CFL Y GT2
	{0x8086,0x87CA,GEN9,"CoffeeLake GT2"},

//	CML GT1
	{0x8086,0x9BA5,GEN9,"CoffeeLake GT1"},
	{0x8086,0x9BA8,GEN9,"CoffeeLake GT1"},
	{0x8086,0x9BA4,GEN9,"CoffeeLake GT1"},
	{0x8086,0x9BA2,GEN9,"CoffeeLake GT1"},

//	CML U GT1
	{0x8086,0x9B21,GEN9,"CoffeeLake GT1"},
	{0x8086,0x9BAA,GEN9,"CoffeeLake GT1"},
	{0x8086,0x9BAC,GEN9,"CoffeeLake GT1"},

//	CML GT2
	{0x8086,0x9BC5,GEN9,"CoffeeLake GT2"},
	{0x8086,0x9BC8,GEN9,"CoffeeLake GT2"},
	{0x8086,0x9BC4,GEN9,"CoffeeLake GT2"},
	{0x8086,0x9BC2,GEN9,"CoffeeLake GT2"},
	{0x8086,0x9BC6,GEN9,"CoffeeLake GT2"},
	{0x8086,0x9BE6,GEN9,"CoffeeLake GT2"},
	{0x8086,0x9BF6,GEN9,"CoffeeLake GT2"},

//	CML U GT2
	{0x8086,0x9B41,GEN9,"CoffeeLake GT2"},
	{0x8086,0x9BCA,GEN9,"CoffeeLake GT2"},
	{0x8086,0x9BCC,GEN9,"CoffeeLake GT2"},

//	CoffeeLake S GT1
	{0x8086,0x3E90,GEN9,"CoffeeLake SRV GT1"},
	{0x8086,0x3E93,GEN9,"CoffeeLake SRV GT1"},
	{0x8086,0x3E99,GEN9,"CoffeeLake SRV GT1"},

//	CoffeeLake S GT2
	{0x8086,0x3E91,GEN9,"CoffeeLake SRV GT2"},
	{0x8086,0x3E92,GEN9,"CoffeeLake SRV GT2"},
	{0x8086,0x3E96,GEN9,"CoffeeLake SRV GT2"},
	{0x8086,0x3E98,GEN9,"CoffeeLake SRV GT2"},
	{0x8086,0x3E9A,GEN9,"CoffeeLake SRV GT2"},

//	CoffeeLake H GT1
	{0x8086,0x3E9C,GEN9,"CoffeeLake Halo GT1"},

//	CoffeeLake H GT2
	{0x8086,0x3E9B,GEN9,"CoffeeLake Halo GT2"},
	{0x8086,0x3E94,GEN9,"CoffeeLake Halo GT2"},

//	CoffeeLake U GT2
	{0x8086,0x3EA9,GEN9,"CoffeeLake ULT GT2"},

//	CoffeeLake U GT3
	{0x8086,0x3EA5,GEN9,"CoffeeLake ULT GT3"},
	{0x8086,0x3EA6,GEN9,"CoffeeLake ULT GT3"},
	{0x8086,0x3EA7,GEN9,"CoffeeLake ULT GT3"},
	{0x8086,0x3EA8,GEN9,"CoffeeLake ULT GT3"},

	{0x8086,0x3EA2,GEN9,"CoffeeLake GT3"},

	{0x8086,0x3EA1,GEN9,"CoffeeLake GT1"},
	{0x8086,0x3EA4,GEN9,"CoffeeLake GT1"},

	{0x8086,0x3EA0,GEN9,"CoffeeLake GT2"},
	{0x8086,0x3EA3,GEN9,"CoffeeLake GT2"},

//	CannonLake F
	{0x8086,0x5A54,GEN10,"CannonLake GT2"},
	{0x8086,0x5A5C,GEN10,"CannonLake GT2"},
	{0x8086,0x5A44,GEN10,"CannonLake GT2"},
	{0x8086,0x5A4C,GEN10,"CannonLake GT2"},

//	CannonLake
	{0x8086,0x5A51,GEN10,"CannonLake GT2"},
	{0x8086,0x5A59,GEN10,"CannonLake GT2"},
	{0x8086,0x5A41,GEN10,"CannonLake GT2"},
	{0x8086,0x5A49,GEN10,"CannonLake GT2"},
	{0x8086,0x5A52,GEN10,"CannonLake GT2"},
	{0x8086,0x5A5A,GEN10,"CannonLake GT2"},
	{0x8086,0x5A42,GEN10,"CannonLake GT2"},
	{0x8086,0x5A4A,GEN10,"CannonLake GT2"},
	{0x8086,0x5A50,GEN10,"CannonLake GT2"},
	{0x8086,0x5A40,GEN10,"CannonLake GT2"},

//	IceLake F
	{0x8086,0x8A50,GEN11,"IceLake"},
	{0x8086,0x8A5C,GEN11,"IceLake"},
	{0x8086,0x8A59,GEN11,"IceLake"},
	{0x8086,0x8A58,GEN11,"IceLake"},
	{0x8086,0x8A52,GEN11,"IceLake"},
	{0x8086,0x8A5A,GEN11,"IceLake"},
	{0x8086,0x8A5B,GEN11,"IceLake"},
	{0x8086,0x8A57,GEN11,"IceLake"},
	{0x8086,0x8A56,GEN11,"IceLake"},
	{0x8086,0x8A71,GEN11,"IceLake"},
	{0x8086,0x8A70,GEN11,"IceLake"},
	{0x8086,0x8A53,GEN11,"IceLake"},
	{0x8086,0x8A54,GEN11,"IceLake"},

//	IceLake 11
	{0x8086,0x8A51,GEN11,"IceLake"},
	{0x8086,0x8A5D,GEN11,"IceLake"},

//	ElkhartLake
	{0x8086,0x4500,GEN11,"ElkhartLake"},
	{0x8086,0x4571,GEN11,"ElkhartLake"},
	{0x8086,0x4551,GEN11,"ElkhartLake"},
	{0x8086,0x4541,GEN11,"ElkhartLake"},
	{0x8086,0x4E71,GEN11,"ElkhartLake"},
	{0x8086,0x4E61,GEN11,"ElkhartLake"},
	{0x8086,0x4E51,GEN11,"ElkhartLake"},

//	TigerLake 12
	{0x8086,0x9A49,GEN12,"TigerLake"},
	{0x8086,0x9A40,GEN12,"TigerLake"},
	{0x8086,0x9A59,GEN12,"TigerLake"},
	{0x8086,0x9A60,GEN12,"TigerLake"},
	{0x8086,0x9A68,GEN12,"TigerLake"},
	{0x8086,0x9A70,GEN12,"TigerLake"},
	{0x8086,0x9A78,GEN12,"TigerLake"},
};

#endif
