# VMware + gdb-stub/vscode-debugger 调试环境配置
#### 本文档简述在Ubuntu平台上，如何使用gdb-stub/vscode-debugger远程调试VMware(架构为x86-64)

### 1.配置VMware

现假定已通过VMware创建了一块虚拟硬盘名为gpt-test, 属于虚拟机grub2-test,那么默认路径应该是 `～/vmware/grub2-test`。
查看该路径下的文件:
```shell
$ ls
drwxrwxr-x 2 cheyh cheyh  286 9月  19 11:21 .
drwxrwxr-x 4 cheyh cheyh   46 5月   6 16:40 ..
-rw-r--r-- 1 cheyh cheyh 8.0G 9月  12 19:23 grub2-test-flat.vmdk
-rw------- 1 cheyh cheyh 265K 9月  12 19:23 grub2-test.nvram
-rw-r--r-- 1 cheyh cheyh  457 5月  27 17:37 grub2-test.vmdk
-rw-r--r-- 1 cheyh cheyh    0 2月  28  2021 grub2-test.vmsd
-rwxr-xr-x 1 cheyh cheyh 2.8K 9月  12 19:23 grub2-test.vmx
-rw-r--r-- 1 cheyh cheyh  265 2月  28  2021 grub2-test.vmxf
```

文本编辑其中后缀为.vmx的文件，建议加入如下两条设置：
```
monitor.debugOnStartGuest64 = "TRUE"	# 虚拟机启动时暂停以等待远程调试器连接
debugStub.listen.guest64 = "TRUE"		# 监听64位客户机端口
```
以下两条根据自己需要，选择加入
```
debugStub.hideBreakpoints = "TRUE"			# 用硬断点代替软断点
debugStub.listen.guest64.remote = "TRUE"	# 允许从另一台机器上调试本虚拟机
```

**建议不要同时开启"listen.guest64"和"listen.guest64.remote"选项，实测同时开启这两项又开启"listen.guest64.remote"会造成vmware无法连接远程调试器**

### 2.gdb-stub远程到该VMware

默认情况下，VMware模拟64位机的远程调试端口为8864(32位为8832)，故gdb输入选项如下设置：  
假定文件名是 `gdb-vmw.txt`，内容如下:
```
set architecture i386:x86-64	# 指定调试目标的架构
target remote localhost:8864	# 远程连接到VMware
file kernel.debug				# 指定调试符号文件
```
如果你为内核编写了独立的应用程序，并希望同时调试应用程序，可通过本文件内加入命令让gdb加载其他调试符号文件:
```
add-symbol-file {path-to-your-app-debug-symbol-file}
```

关于调试符号文件的获取，在 makefile 的提取裸二进制文件 kernel.bin 那一行命令：
```shell
objcopy -I elf64-x86-64 -S -R ".eh_frame" -R ".comment" -O binary system kernel.bin
```
之前，加入命令：
```shell
x86_64-elf-objcopy --only-keep-debug system kernel.debug
```
即可获得此调试符号文件。

在启动VMware后，通过命令 `gdb -x ./gdb-vmw.txt` 启动gdb并远程到VMware上进行调试。
**建议先启动vmware再启动gdb，若先启动gdb后再启动vmware会造成vmware卡死。**

### 3.vscode-debugger一键启动调试
利用vscode自身的debug模块可以进行一键启动虚拟机+debug-stub远程连接虚拟机+开始调试

**首先配置VMware虚拟机设置:**  
必须开启选项:
```
monitor.debugOnStartGuest64 = "TRUE"
```
必须禁用选项:
```
# debugStub.listen.guest64 = "TRUE"
# debugStub.listen.guest64.remote = "TRUE"
```
  
**然后配置vscode**  
用vscode打开项目后会在项目根目录下自动生成　`.vscode`　目录，将本文档目录中的 `launch.json` 和 `tasks.json` 放入你的 `.vscode` 目录内。
然后将　`launch.json` 中的 `program` 选项, `tasks.json` 中的 `command` 选项值替换为你自己的符号文件和虚拟机配置文件的路径。两个json文件中的关键项含义有注释说明，也可自行查阅[官方文档](https://code.visualstudio.com/docs/cpp/launch-json-reference)。`launch.json` 中 `setupCommands` 选项组中加入的选项就是本文档第二节中 `gdb-vmw.txt` 文件中的内容，每行为一个子条目。