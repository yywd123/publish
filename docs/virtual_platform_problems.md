本列表列出开发过程中使用过的调试平台的问题和注意事项

**bochs**:截至2.6.11，仍不支持UEFI启动  

**qemu**:启用kvm后，gdb-stub软件断点无法命中，需要用hb打断点，但不开启kvm则不支持一些架构特性比如x2APIC  

**VMware**:对特权寄存器有保护，所vscode的debug-session的寄存器列表,gdb命令行中通常用的　`i reg`　(打印寄存器值)命令不会打印诸如cr3的特权寄存器的值，需要专用命令　`monitor reg cr3`  
