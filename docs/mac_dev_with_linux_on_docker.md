# 在Mac系统下用docker安装Linux调试内核的方法

## 初始化Linux开发环境

安装docker后，安装Linux。本说明用的是Fedora，也可以用其它发行版。

 1. 拉取fedora镜像： `docker pull fedora`
 2. 启动Fedora同时将本地目录`~/docker`映射到Fedora的`/docker`(可以视情况自己安排): `docker run -ivt ~/docker:/docker --name kernel fedora bash`
 3. dnf 安装 gcc nasm vim kpartx git e2fsprogs.x86_64 等必要软件环境
 4. 编译内核kernel和user

## 初始化硬盘镜像文件

创建一个1G的硬盘镜像文件`x.img`

`dd if=/dev/zero of=x.img bs=512 count=2097152`

创建GPT和UEFI，以及创建FAT32分区的工作可以参考[这篇文档](./about_virtual_disk.txt)

以上工作完成后，执行以下命令

```
# 安装UEFI文件
mkdir -p /mnt/EFI/BOOT/
cp UEFIBootLoader/BootLoader.efi /mnt/EFI/BOOT/BOOTX64.EFI

#卸载硬盘镜像文件
umount /mnt
# loop的xxx编号视具体情况而定
kpartx -d /dev/loopxxx
losetup -d /dev/loopxxx
```
然后就得到一个初始化好的硬盘镜像文件。

## 调试内核

接下来就是，然后将生成的文件拷贝进`x.img`

拷贝可以执行scripts/copy_bins.sh，这个脚本直接默认你用root权限执行，如果是其它Linux系统可能需要添加sudo。

以后每次编译新的内核，都只需要执行一次copy_bins.sh就可以了。

生成的x.img文件可以在Mac系统的`~/docker`目录里找到，执行qemu即可开始调试。如果直接在Linux系统开发，就更简单，直接启动qemu就可。

qemu命令: `qemu-system-x86_64 -bios OVMF.fd -hdb x.img -m 2048  -smp threads=1,cores=1,sockets=1 -vga virtio -name UEFI -boot order=dc -net none`

其中OVMF.fd文件可以在ubuntu上面`apt-get install ovmf`，然后在`/usr/share/ovmf/`找到这个文件。