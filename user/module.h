/***************************************************
*		 Copyright (c) 2018 MINE 田宇
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of version 2 of the GNU General Public
* License as published by the Free Software Foundation.
*
***************************************************/

#ifndef __MODULE_H__

#define __MODULE_H__

long init_module(int fildes);
long delete_module(const char *module_name);

#endif
